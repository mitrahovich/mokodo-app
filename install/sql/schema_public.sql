CREATE TABLE public.data_source_list
(
  id BIGSERIAL NOT NULL PRIMARY KEY,
  title text,
  src text,
  file_db text,
  created timestamp(0) without time zone DEFAULT now(),
  updated timestamp(0) without time zone
);

CREATE TABLE public.fos_user (
  id SERIAL NOT NULL PRIMARY KEY,
  username VARCHAR(180) NOT NULL,
  username_canonical VARCHAR(180) NOT NULL UNIQUE,
  email VARCHAR(180) NOT NULL,
  email_canonical VARCHAR(180) NOT NULL UNIQUE,
  enabled BOOLEAN NOT NULL,
  salt VARCHAR(255) DEFAULT NULL,
  password VARCHAR(255) NOT NULL,
  last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  confirmation_token VARCHAR(180) DEFAULT NULL UNIQUE,
  password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
  roles TEXT NOT NULL
);