<?php

namespace ReportsBundle\Controller;

use AppBundle\Entity\DSContract;
use AppBundle\Entity\DSContractConformity;
use AppBundle\Event\EditFlagContractEvent;
use ReportsBundle\Manager\ReportsManager;
use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContractsController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     *
     * @param Request $request
     *
     * @Route("/contracts", name="contracts")
     * @Template()
     * @Method("GET")
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));
        $ds = $reportManager->getCurrentDS($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('reports.datatable.contracts');
        $datatable->changeEntityManager(
            DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em)
        );

        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        }

        return ['datatable' => $datatable];
    }

    /**
     *
     * @param Request $request
     *
     * @Route("/contracts/view/{id}", name="contracts-view", requirements={
     * "id": "\d+"
     * })
     * @Template()
     * @Method("GET")
     *
     * @return Response
     */
    public function viewAction(Request $request, $id)
    {
        $isAjax = $request->isXmlHttpRequest();

        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));
        $ds = $reportManager->getCurrentDS($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('card.datatable.contracts');
        $liteEm = DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em);
        $datatable->changeEntityManager($liteEm);


        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $company = $liteEm->find(\AppBundle\Entity\DSCompanyList::class, $id);

            /** @var QueryBuilder $qb */
            $qb = $datatableQueryBuilder->getQb();
            $qb->andWhere('contracts.companyList = :company');
            $qb->setParameter('company', $company);

            return $responseService->getResponse();
        }

        return ['datatable' => $datatable];
    }


    /**
     *
     * @param Request $request
     *
     * @Route("/contracts/edit/satisfied", name="contracts_satisfied_edit")
     * @Template()
     * @Method("POST")
     *
     * @return array
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function editFieldSatisfiedAction(Request $request): array
    {
        /** @var ReportsManager $reportManager */
        /** @var EntityManager $em */

        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));

        $ds = $reportManager->getCurrentDS($request);

        if (null === $ds) {
            throw new \LogicException('DATA SOURCE NOT FOUND');
        }

        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('reports.datatable.company');
        $liteEm = DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em);
        $datatable->changeEntityManager($liteEm);


        $excess =  $liteEm->find(DSContractConformity::class, $request->request->get('pk'));
        if(!($excess instanceof DSContractConformity)) {
            $excess = new DSContractConformity();
        }

        $contract = $liteEm->find(DSContract::class, $request->request->get('pk'));

        if (null === $contract) {
            throw new \LogicException('CONTRACT NOT FOUND ID - ' . $request->request->get('pk'));
        }

        $excess->setContract($contract);
        $excess->setConformity($request->request->get('value'));
        $liteEm->persist($excess);

        $liteEm->flush();

        $eventDispatcher = $this->get('event_dispatcher');
        $eventDispatcher->dispatch('edit_flag_contract', new EditFlagContractEvent(
            $liteEm, $contract, $this->getUser(), $request->request->get('value'))
        );

        $liteEm->flush();

        return [];
    }
}
