<?php

namespace ReportsBundle\Controller;

use AppBundle\AppBundle;
use ReportsBundle\Manager\ReportsManager;
use AppBundle\Manager\DataSourceManager;
use AppBundle\Manager\DSCompanyManager;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\DataSource;
use AppBundle\Entity\DSCompanyExcessKfodo;
use AppBundle\Form\Type\DataSourceType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class KFODOReportController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     *
     * @param Request $request
     *
     * @Route("/report/kfodo", name="report_bkfodo")
     * @Template()
     * @Method("GET")
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));
        $ds = $reportManager->getCurrentDS($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('reports.datatable.kfodo');
        $datatable->changeEntityManager(
            DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em)
        );

        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        }

        return ['datatable' => $datatable];
    }


    /**
     *
     * @param Request $request
     *
     * @Route("/report/kfodo/edit/excess", name="kfodo_excess_edit")
     * @Template()
     * @Method("POST")
     *
     * @return Response
     */
    public function editFieldExcessAction(Request $request)
    {
        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));

        $ds = $reportManager->getCurrentDS($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('reports.datatable.company');
        $liteEm = DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em);
        $datatable->changeEntityManager($liteEm);


        $excess =  $liteEm->find(\AppBundle\Entity\DSCompanyExcessKfodo::class, $request->request->get('pk'));
        if(!($excess instanceof \AppBundle\Entity\DSCompanyExcessKfodo)) {
            $excess = new DSCompanyExcessKfodo();
        }
        $company = $liteEm->find(\AppBundle\Entity\DSCompanyList::class, $request->request->get('pk'));
        $excess->setCompany($company);
        $excess->setIsExcess($request->request->get('value'));
        $liteEm->persist($excess);
        $liteEm->flush();

        return [];
    }

}
