<?php
namespace ReportsBundle\Controller;


use AppBundle\Entity\DataSource;
use AppBundle\Event\EditFlagCompanyEvent;
use AppBundle\Manager\DataSourceManager;
use AppBundle\Manager\DSCompanyManager;
use Doctrine\ORM\EntityManager;
use ReportsBundle\Datatables\DSCompanyListDatatable;
use ReportsBundle\Manager\ReportsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ReportsController extends Controller
{
    /**
     * Тестовый грид
     *
     * @param Request $request
     *
     * @Route("/company/list", name="current_company_list")
     * @Template()
     * @Method("GET")
     *
     * @return Response
    */
    public function indexAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));

        $ds = $reportManager->getCurrentDS($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('reports.datatable.company');
        $datatable->changeEntityManager(
            DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em)
        );

        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        }

        return [
            'ds_name' => $ds->getTitle(),
            'datatable' => $datatable
        ];
    }


    /**
     *
     * @param Request $request
     *
     * @Route("/company/field", name="company_field_level_edit")
     * @Template()
     * @Method("POST")
     *
     * @return Response
     */
    public function editFieldLevelAction(Request $request)
    {
        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));

        $ds = $reportManager->getCurrentDS($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('reports.datatable.company');
        $liteEm = DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em);
        $datatable->changeEntityManager($liteEm);

        $company = $liteEm->find(\ReportsBundle\Entity\DSCompanyList::class, $request->request->get('pk'));
        $level = $liteEm->find(\AppBundle\Entity\DSDictLevel::class, $request->request->get('value'));
        $company->setLevel($level);

        //$liteEm->persist($company);
        $liteEm->flush();

        $eventDispatcher = $this->get('event_dispatcher');
        $eventDispatcher->dispatch('edit_flag_company', new EditFlagCompanyEvent($company, $this->getUser(), $request->request->get('value')));

        return [];
    }

    /**
     *
     * @param Request $request
     *
     * @Route("/company/field_vv", name="company_field_vv_level_edit")
     * @Template()
     * @Method("POST")
     *
     * @return Response
     */
    public function editFieldVVLevelAction(Request $request)
    {
        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));

        $ds = $reportManager->getCurrentDS($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('reports.datatable.company');
        $liteEm = DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em);
        $datatable->changeEntityManager($liteEm);

        $company = $liteEm->find(\ReportsBundle\Entity\DSCompanyList::class, $request->request->get('pk'));
        $level = $liteEm->find(\AppBundle\Entity\DSDictVVLevel::class, $request->request->get('value'));
        $company->setLevelVV($level);

        //$liteEm->persist($company);
        $liteEm->flush();

        $eventDispatcher = $this->get('event_dispatcher');
        $eventDispatcher->dispatch('edit_flag_company', new EditFlagCompanyEvent($company, $this->getUser(), $request->request->get('value')));

        return [];
    }
}
