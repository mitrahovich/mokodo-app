<?php

namespace ReportsBundle\Controller;

use AppBundle\Entity\DSCourtCaseConformity;

use AppBundle\Event\EditFlagCourtCaseEvent;
use AppBundle\Filter\Filter;
use Doctrine\DBAL\Query\QueryBuilder;
use ReportsBundle\Entity\DSCompanyList;
use ReportsBundle\Manager\ReportsManager;
use AppBundle\Manager\DataSourceManager;
use AppBundle\Manager\DSCompanyManager;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\DataSource;
use AppBundle\Form\Type\DataSourceType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CourtCasesController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     *
     * @param Request $request
     *
     * @Route("/report/court-cases", name="report_court-cases")
     * @Template()
     * @Method("GET")
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));
        $ds = $reportManager->getCurrentDS($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('reports.datatable.courtcases');
        $datatable->changeEntityManager(
            DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em)
        );

        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        }

        return ['datatable' => $datatable];
    }

    /**
     *
     * @param Request $request
     *
     * @Route("/court-cases/view/{id}", name="court-cases", requirements={
     * "id": "\d+"
     * })
     * @Template()
     * @Method("GET")
     *
     * @return Response
     */
    public function viewAction(Request $request, $id)
    {
        $isAjax = $request->isXmlHttpRequest();

        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));
        $ds = $reportManager->getCurrentDS($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('card.datatable.courtcases');
        $liteEm = DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em);
        $datatable->changeEntityManager($liteEm);


        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            $company = $liteEm->find(\AppBundle\Entity\DSCompanyList::class, $id);

            /** @var QueryBuilder $qb */
            $qb = $datatableQueryBuilder->getQb();
            $qb->andWhere('vwcourtcase.companyList = :company');
            $qb->setParameter('company', $company);

            return $responseService->getResponse();
        }

        return ['datatable' => $datatable];
    }

    /**
     *
     * @param Request $request
     *
     * @Route("/court-cases/edit/satisfied", name="court_cases_satisfied_edit")
     * @Template()
     * @Method("POST")
     *
     * @return Response
     */
    public function editFieldSatisfiedAction(Request $request)
    {
        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));

        $ds = $reportManager->getCurrentDS($request);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('reports.datatable.company');
        $liteEm = DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em);
        $datatable->changeEntityManager($liteEm);


        $excess =  $liteEm->find(\AppBundle\Entity\DSCourtCaseConformity::class, $request->request->get('pk'));
        if(!($excess instanceof \AppBundle\Entity\DSCourtCaseConformity)) {
            $excess = new DSCourtCaseConformity();
        }
        $courtCase = $liteEm->find(\AppBundle\Entity\DSCourtCase::class, $request->request->get('pk'));
        $excess->setCourtCase($courtCase);
        $excess->setConformity($request->request->get('value'));
        $liteEm->persist($excess);
        $liteEm->flush();
        $eventDispatcher = $this->get('event_dispatcher');
        $eventDispatcher->dispatch('edit_flag_courtcase', new EditFlagCourtCaseEvent($courtCase, $this->getUser(), $request->request->get('value')));

        return [];
    }

}
