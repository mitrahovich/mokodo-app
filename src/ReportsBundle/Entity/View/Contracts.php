<?php
namespace ReportsBundle\Entity\View;

use AppBundle\Entity\Traits\TimeStampableTrait;
use Doctrine\ORM\Mapping as ORM;
use ReportsBundle\Entity\DSCompanyList;

/**
 * Contracts
 *
 * @ORM\Table(name="vw_contracts")
 * @ORM\Entity(readOnly=true)
 */
class Contracts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string")
     *
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     *
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_contract",type="integer")
     */
    private $idContract;

    /**
     * @var int
     *
     * @ORM\Column(name="okdp",type="integer")
     */
    private $okdp;

    /**
     * @var string
     *
     * @ORM\Column(name="desc", type="string")
     *
     */
    private $desc;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="contract_date",type="datetime")
     */
    private $contractDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="plan_date",type="datetime")
     */
    private $planDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fact_date",type="datetime")
     */
    private $factDate;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string")
     *
     */
    private $level;


    /**
     * @var int
     *
     * @ORM\Column(name="contract_summ", type="decimal", scale=12, precision=2, nullable=true)
     */
    private $contractSumm;

    /**
     * @var int
     *
     * @ORM\Column(name="kfm_summ", type="decimal", scale=12, precision=2, nullable=true)
     */
    private $kfmSumm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="satisfied", type="boolean")
     */
    private $satisfied;


    /**
     * @var DSCompanyList
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCompanyList")
     * @ORM\JoinColumn(name="company_list_id", referencedColumnName="id")
     *
     */
    private $companyList;

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getIdContract(): int
    {
        return $this->idContract;
    }

    /**
     * @param int $idContract
     */
    public function setIdContract(int $idContract)
    {
        $this->idContract = $idContract;
    }

    /**
     * @return int
     */
    public function getOkdp(): int
    {
        return $this->okdp;
    }

    /**
     * @param int $okdp
     */
    public function setOkdp(int $okdp)
    {
        $this->okdp = $okdp;
    }

    /**
     * @return string
     */
    public function getDesc(): string
    {
        return $this->desc;
    }

    /**
     * @param string $desc
     */
    public function setDesc(string $desc)
    {
        $this->desc = $desc;
    }

    /**
     * @return \DateTime
     */
    public function getContractDate(): \DateTime
    {
        return $this->contractDate;
    }

    /**
     * @param \DateTime $contractDate
     */
    public function setContractDate(\DateTime $contractDate)
    {
        $this->contractDate = $contractDate;
    }

    /**
     * @return \DateTime
     */
    public function getPlanDate(): \DateTime
    {
        return $this->planDate;
    }

    /**
     * @param \DateTime $planDate
     */
    public function setPlanDate(\DateTime $planDate)
    {
        $this->planDate = $planDate;
    }

    /**
     * @return \DateTime
     */
    public function getFactDate(): \DateTime
    {
        return $this->factDate;
    }

    /**
     * @param \DateTime $factDate
     */
    public function setFactDate(\DateTime $factDate)
    {
        $this->factDate = $factDate;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel(string $level)
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getContractSumm(): int
    {
        return $this->contractSumm;
    }

    /**
     * @param int $contractSumm
     */
    public function setContractSumm(int $contractSumm)
    {
        $this->contractSumm = $contractSumm;
    }

    /**
     * @return int
     */
    public function getKfmSumm(): int
    {
        return $this->kfmSumm;
    }

    /**
     * @param int $kfmSumm
     */
    public function setKfmSumm(int $kfmSumm)
    {
        $this->kfmSumm = $kfmSumm;
    }

    /**
     * @return bool
     */
    public function isSatisfied(): bool
    {
        return $this->satisfied;
    }

    /**
     * @param bool $satisfied
     */
    public function setSatisfied(bool $satisfied)
    {
        $this->satisfied = $satisfied;
    }

    /**
     * @return DSCompanyList
     */
    public function getCompanyList(): DSCompanyList
    {
        return $this->companyList;
    }


}