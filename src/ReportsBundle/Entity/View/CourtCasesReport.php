<?php
namespace ReportsBundle\Entity\View;

use AppBundle\Entity\Traits\TimeStampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * CourtCasesReport
 *
 * @ORM\Table(name="vw_court_cases")
 * @ORM\Entity(readOnly=true)
 */
class CourtCasesReport
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string")
     *
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     *
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="clericalwork_count",type="integer")
     */
    private $clericalworkCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="clericalwork_last_date",type="datetime")
     */
    private $clericalworkLastDate;

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getClericalworkCount(): int
    {
        return $this->clericalworkCount;
    }

    /**
     * @param int $clericalworkCount
     */
    public function setClericalworkCount(int $clericalworkCount)
    {
        $this->clericalworkCount = $clericalworkCount;
    }
    /**
     * @return \DateTime
     */
    public function getClericalworkLastDate(): \DateTime
    {
        return $this->clericalworkLastDate;
    }

    /**
     * @param \DateTime $clericalworkLastDate
     */
    public function setClericalworkLastDate(\DateTime $clericalworkLastDate)
    {
        $this->clericalworkLastDate = $clericalworkLastDate;
    }


}