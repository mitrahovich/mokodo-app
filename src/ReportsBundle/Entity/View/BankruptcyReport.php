<?php
namespace ReportsBundle\Entity\View;

use AppBundle\Entity\Traits\TimeStampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * CourtCasesReport
 *
 * @ORM\Table(name="vw_bankruptcy")
 * @ORM\Entity(readOnly=true)
 */
class BankruptcyReport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string")
     *
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     *
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="procedure", type="string")
     *
     */
    private $procedure;

    /**
     * @var string
     *
     * @ORM\Column(name="work_number", type="string")
     *
     */
    private $workNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="work_date",type="datetime")
     */
    private $workDate;

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getProcedure(): string
    {
        return $this->procedure;
    }

    /**
     * @param string $procedure
     */
    public function setProcedure(string $procedure)
    {
        $this->procedure = $procedure;
    }

    /**
     * @return string
     */
    public function getWorkNumber(): string
    {
        return $this->workNumber;
    }

    /**
     * @param string $workNumber
     */
    public function setWorkNumber(string $workNumber)
    {
        $this->workNumber = $workNumber;
    }

    /**
     * @return \DateTime
     */
    public function getWorkDate(): \DateTime
    {
        return $this->workDate;
    }

    /**
     * @param \DateTime $workDate
     */
    public function setWorkDate(\DateTime $workDate)
    {
        $this->workDate = $workDate;
    }


}