<?php
namespace ReportsBundle\Entity\View;

use Doctrine\ORM\Mapping as ORM;

/**
 * KFVVReport
 *
 * @ORM\Entity
 * @ORM\Table(name="vw_kf_vv")
 */
class KFVVReport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string")
     *
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     *
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="level",type="integer")
     */
    private $level;

    /**
     * @var int
     *
     * @ORM\Column(name="limit_summ", type="decimal", scale=12, precision=2, nullable=true)
     */
    private $limitSumm;


    /**
     * @var int
     *
     * @ORM\Column(name="contract_max_summ", type="decimal", scale=12, precision=2, nullable=true)
     */
    private $contractMaxSumm;


    /**
     * @var boolean
     *
     * @ORM\Column(name="excess", type="boolean")
     */
    private $excess;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @return int
     */
    public function getLimitSumm(): int
    {
        return $this->limitSumm;
    }


    /**
     * @return int
     */
    public function getContractMaxSumm(): int
    {
        return $this->contractMaxSumm;
    }

    /**
     * @return bool
     */
    public function isExcess(): bool
    {
        return $this->excess;
    }

}