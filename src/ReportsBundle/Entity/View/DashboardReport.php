<?php
namespace ReportsBundle\Entity\View;

use AppBundle\Entity\DSDictLevel;
use Doctrine\ORM\Mapping as ORM;

/**
 * DashboardReport
 *
 * @ORM\Table(name="wv_dashboard_report")
 * @ORM\Entity
 */
class DashboardReport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string")
     *
     */
    private $inn;

    /**
     * @var DSDictLevel
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSDictLevel")
     * @ORM\JoinColumn(name="dict_level_id", referencedColumnName="id")
     *
     */
    private $level;

    /**
     * @var DSDictLevel
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSDictVVLevel")
     * @ORM\JoinColumn(name="dict_vv_level_id", referencedColumnName="id")
     *
     */
    private $levelVV;

    /**
     * @var int
     *
     * @ORM\Column(name="court_cases_count", type="decimal", scale=12, precision=2, nullable=true)
     */
    private $courtCasesCount;

    /**
     * @var int
     *
     * @ORM\Column(name="contract_count", type="decimal", scale=12, precision=2, nullable=true)
     */
    private $contractCount;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     *
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="contract_max_summ", type="decimal", scale=12, precision=2, nullable=true)
     */
    private $contractMaxSumm;

    /**
     * @var int
     *
     * @ORM\Column(name="contract_summ", type="decimal", scale=12, precision=2, nullable=true)
     */
    private $contractSumm;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * @return DSDictLevel
     */
    public function getLevel(): DSDictLevel
    {
        return $this->level;
    }

    /**
     * @return DSDictLevel
     */
    public function getLevelVV(): DSDictLevel
    {
        return $this->levelVV;
    }

    /**
     * @return int
     */
    public function getCourtCasesCount(): int
    {
        return $this->courtCasesCount;
    }

    /**
     * @return int
     */
    public function getContractCount(): int
    {
        return $this->contractCount;
    }

    /**
     * @return int
     */
    public function getContractMaxSumm(): int
    {
        return $this->contractMaxSumm;
    }


}