<?php
namespace ReportsBundle\Entity\View;

use AppBundle\Entity\Traits\TimeStampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * ExecutionContractReport
 *
 * @ORM\Table(name="vw_execution_contract")
 * @ORM\Entity(readOnly=true)
 */
class ExecutionContractReport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string")
     *
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     *
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="failed_contract_count",type="integer")
     */
    private $failedContractCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="failed_contract_last_date",type="datetime")
     */
    private $failedContractLastDate;

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getFailedContractCount(): int
    {
        return $this->failedContractCount;
    }

    /**
     * @param int $failedContractCount
     */
    public function setFailedContractCount(int $failedContractCount)
    {
        $this->failedContractCount = $failedContractCount;
    }

    /**
     * @return \DateTime
     */
    public function getFailedContractLastDate(): \DateTime
    {
        return $this->failedContractLastDate;
    }

    /**
     * @param \DateTime $failedContractLastDate
     */
    public function setFailedContractLastDate(\DateTime $failedContractLastDate)
    {
        $this->failedContractLastDate = $failedContractLastDate;
    }

}