<?php
namespace ReportsBundle\Entity;

use AppBundle\Entity\DSDictLevel;
use AppBundle\Entity\DSDictVVLevel;
use AppBundle\Entity\Traits\TimeStampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * DSCompanyList
 *
 * @ORM\Table(name="company_list")
 * @ORM\Entity
 */
class DSCompanyList
{
    use TimeStampableTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string")
     *
     */
    private $inn;

    /**
     * @var DSDictLevel
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSDictLevel")
     * @ORM\JoinColumn(name="dict_level_id", referencedColumnName="id")
     *
     */
    private $level;

    /**
     * @var DSDictVVLevel
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSDictVVLevel")
     * @ORM\JoinColumn(name="dict_vv_level_id", referencedColumnName="id")
     *
     */
    private $levelVV;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     *
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="sync", type="boolean", nullable=false)
     */
    private $isSync;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isSync(): bool
    {
        return $this->isSync;
    }

    /**
     * @param bool $isSync
     */
    public function setIsSync(bool $isSync)
    {
        $this->isSync = $isSync;
    }

    public function getSyncToString()
    {
        return ($this->isSync())?"Да":"Нет";
    }

    /**
     * @return DSDictLevel
     */
    public function getLevel(): DSDictLevel
    {
        return $this->level;
    }

    /**
     * @param DSDictLevel $level
     */
    public function setLevel(DSDictLevel $level)
    {
        $this->level = $level;
    }

    /**
     * @return DSDictVVLevel
     */
    public function getLevelVV(): DSDictVVLevel
    {
        return $this->levelVV;
    }

    /**
     * @param DSDictVVLevel $levelVV
     */
    public function setLevelVV(DSDictVVLevel $levelVV)
    {
        $this->levelVV = $levelVV;
    }

}