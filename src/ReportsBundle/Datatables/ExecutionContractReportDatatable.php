<?php

namespace ReportsBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class ExecutionContractReportDatatable
 *
 * @package ReportsBundle\Datatables
 */
class ExecutionContractReportDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true,
            'language' => 'ru'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
            'classes' => Style::BOOTSTRAP_3_STYLE .' dt',
            //'classes' => 'dt',
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'dom' => 'Bfrtip',
            'length_menu' => [
                [ 10, 25, 50, -1 ],
                [ '10 строк', '25 строк', '50 строк', 'Показать все' ]
            ]
        ));

        $this->callbacks->set(array(
            'row_callback' => array(
                'template' => ':bankruptcy:row_callback.js.twig',
                'vars' => array('route' => '/company/view/'),
            )
        ));

        $this->features->set(array(
            'processing' => true,
            'length_change' => true
        ));

        $this->extensions->set(array(
            //'responsive' => true,
            'buttons' => array(
                'create_buttons' => array(
                    [
                        'extend' => 'excelHtml5',
                        'text' => 'Excel',
                        'button_options'=>[
                            'exportOptions' =>[
                                'columns'=>':visible'////$this->getPdfColumns()
                            ]
                        ]
                    ],

                    ['extend' => 'pageLength']

                ),
            ),
            'responsive' => array(
                'details' => array(
                    'display' => array(
                        'template' => ':extension:display.js.twig',
                    ),
                    'renderer' => array(
                        'template' => ':extension:renderer.js.twig',
                    ),
                ),
            ),
        ));

        $this->columnBuilder
            ->add('name', Column::class, array(
                'title' => 'Наименование организации',
                'width' => '100%',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
            ->add('inn', Column::class, array(
                'title' => 'ИНН',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
            ->add('failedContractCount', Column::class, array(
                'title' => 'Количество невыполненных контрактов',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
            ->add('failedContractLastDate', DateTimeColumn::class, array(
                'title' => 'Дата последнего невыполненного контракта',
                'filter' => array(DateRangeFilter::class,
                    array(
                        'cancel_button' => false,
                        'classes' => 'form-control'
                    ),
                ),
            ))

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'ReportsBundle\Entity\View\ExecutionContractReport';
    }

    /**
     * {@inheritdoc}
     */
    public function changeEntityManager($em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'execution_contract_datatable';
    }
    /**
     * Returns the columns which are to be displayed in a pdf.
     *
     * @return array
     */
    private function getPdfColumns()
    {
        return array(
            '0', '1', '2','3'
        );

    }
}
