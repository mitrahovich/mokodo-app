<?php

namespace ReportsBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\NumberColumn;
use Sg\DatatablesBundle\Datatable\Filter\Select2Filter;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class CompanyContractsDatatable
 *
 * @package ReportsBundle\Datatables
 */
class CompanyContractsDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true,
            'language' => 'ru'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
        'classes' => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'dom' => 'Bfrtip',
            'length_menu' => [
                [ 10, 25, 50, -1 ],
                [ '10 строк', '25 строк', '50 строк', 'Показать все' ]
            ]
        ));

        $this->callbacks->set(array(
            'row_callback' => array(
                'template' => ':bankruptcy:row_callback.js.twig',
                'vars' => array('route' => '/contract/view/'),
            )
        ));

        $this->features->set(array(
            'processing' => true,
            'length_change' => true,
            'scroll_x' => true
        ));

        $this->extensions->set(array(
            //'responsive' => true,
            'buttons' => array(
                'create_buttons' => array(
                    [
                        'extend' => 'excelHtml5',
                        'text' => 'Excel',
                        'button_options'=>[
                            'exportOptions' =>[
                                'columns'=>':visible'//$this->getPdfColumns()
                            ]
                        ]
                    ],

                   ['extend' => 'pageLength']

                ),
            ),
            'responsive' => array(
                'details' => array(
                    'display' => array(
                        'template' => ':extension:display.js.twig',
                    ),
                    'renderer' => array(
                        'template' => ':extension:renderer.js.twig',
                    ),
                ),
            ),
        ));

        $formatter = new \NumberFormatter('ru_RU', \NumberFormatter::CURRENCY);

        $this->columnBuilder
            ->add('idContract', Column::class, array(
                'title' => 'ID контракта',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
            ->add('desc', Column::class, array(
                'title' => 'Описание',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
            ->add('contractDate', DateTimeColumn::class, array(
                'title' => 'Дата заключения',
                'filter' => array(DateRangeFilter::class, ['classes' => 'form-control',])
            ))
            ->add('factDate', DateTimeColumn::class, array(
                'title' => 'Дата исполнения',
                'filter' => array(DateRangeFilter::class, ['classes' => 'form-control',])
            ))
            ->add('contractSumm', NumberColumn::class, array(
                'title' => 'Сумма по текущему контракту',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',]),
                'formatter' => $formatter,
                'use_format_currency' => true
            ))
/*            ->add('sum_kfm', Column::class, array(
                'title' => 'Сумма текущей ответственности(KFM)',
                'dql' => '(SELECT sum({p}) FROM ReportsBundle:View\Contracts {p} WHERE {p}.law_number=223-ФЗ)',
                'searchable' => true,
                'orderable' => true,
            ))*/
            ->add('satisfied', BooleanColumn::class, array(
                'title' => 'Отметка о соответствии контракта',
                'searchable' => true,
                'orderable' => true,
                'true_label' => 'Да',
                'false_label' => 'Нет',
                'default_content' => '-',
                'true_icon' => '',
                'false_icon' => '',
                'filter' => array(Select2Filter::class, array(
                    'classes' => 'form-control',
                    'search_type' => 'eq',
                    'multiple' => true,
                    'select_options' => array(
                        '' => 'Все',
                        '1' => 'Да',
                        '0' => 'Нет'
                    ),
                    'cancel_button' => false,
                )),
                'editable' => array(SelectEditable::class, array(
                    'url' => 'contracts_satisfied_edit',
                    'source' => array(
                        array('value' => 1, 'text' => 'Да'),
                        array('value' => 0, 'text' => 'Нет')
                    ),
                    'mode' => 'inline',
                    //'pk' => 'cid',
                    'empty_text' => '',
                )),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'ReportsBundle\Entity\View\Contracts';
    }

    /**
     * {@inheritdoc}
     */
    public function changeEntityManager($em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dscompanycontracts_datatable';
    }
    /**
     * Returns the columns which are to be displayed in a pdf.
     *
     * @return array
     */
    private function getPdfColumns()
    {
            return array(
                '0', '1', '2','3','4','5','6'
            );

    }
}
