<?php

namespace ReportsBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Column\NumberColumn;
use Sg\DatatablesBundle\Datatable\Filter\Select2Filter;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class CourtCasesDatatable
 *
 * @package ReportsBundle\Datatables
 */
class CourtCasesDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true,
            'language' => 'ru'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
        'classes' => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'dom' => 'Bfrtip',
            'length_menu' => [
                [ 10, 25, 50, -1 ],
                [ '10 строк', '25 строк', '50 строк', 'Показать все' ]
            ]
        ));

        $this->features->set(array(
            'processing' => true,
            'length_change' => true,
            'scroll_x' =>true,
            'auto_width'=> true
        ));

        $this->callbacks->set(array(
            'row_callback' => array(
                'template' => ':bankruptcy:row_callback.js.twig',
                'vars' => array('route' => '/court-case/view/'),
            )
        ));

        $this->extensions->set(array(
            //'responsive' => true,
            'buttons' => array(
                'create_buttons' => array(
                    [
                        'extend' => 'excelHtml5',
                        'text' => 'Excel',
                        'button_options'=>[
                            'exportOptions' =>[
                                'columns'=>':visible'//$this->getPdfColumns()
                            ]
                        ]
                    ],

                   ['extend' => 'pageLength']

                ),
            ),
            'responsive' => array(
                'details' => array(
                    'display' => array(
                        'template' => ':extension:display.js.twig',
                    ),
                    'renderer' => array(
                        'template' => ':extension:renderer.js.twig',
                    ),
                ),
            ),
        ));

        $formatter = new \NumberFormatter('ru_RU', \NumberFormatter::CURRENCY);

        $this->columnBuilder
            ->add('defendant', Column::class, array(
                'title' => 'Наименование организации',
                'width' => '100%',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
            ->add('defendantInn', Column::class, array(
                'title' => 'ИНН',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
            ->add('caseNumber', Column::class, array(
                'title' => 'Номер дела',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
            ->add('claimant', Column::class, array(
                'title' => 'Истец',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
            ->add('caseType', Column::class, array(
                'title' => 'Описание',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
/*            ->add('caseType', Column::class, array(
                'title' => 'Тип участия',
            ))*/
            ->add('instanceDate', DateTimeColumn::class, array(
                'title' => 'Дата арбитража',
                'filter' => array(DateRangeFilter::class, ['classes' => 'form-control',])
            ))
            ->add('isActive', BooleanColumn::class, array(
                'title' => 'Является активным',
                'filter' => array(Select2Filter::class, array(
                    'classes' => 'form-control',
                    'search_type' => 'eq',
                    'multiple' => false,
                    'select_options' => array(
                        '' => '',
                        '1' => 'Да',
                        '0' => 'Нет'
                    ),
                    'cancel_button' => false,
                )),
            ))
            ->add('sum', NumberColumn::class, array(
                'title' => 'Сумма по делу',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',]),
                'formatter' => $formatter,
                'use_format_currency' => true
            ))
            ->add('satisfied', BooleanColumn::class, array(
                'title' => 'Отметка о соответствии арбтражного строительству',
                'searchable' => true,
                'orderable' => true,
                'true_label' => 'Да',
                'false_label' => 'Нет',
                'default_content' => '-',
                'true_icon' => '',
                'false_icon' => '',
                'filter' => array(Select2Filter::class, array(
                    'classes' => 'form-control',
                    'search_type' => 'eq',
                    'multiple' => true,
                    'select_options' => array(
                        '' => 'Все',
                        '1' => 'Да',
                        '0' => 'Нет'
                    ),
                    'cancel_button' => false,
                )),
                'editable' => array(SelectEditable::class, array(
                    'url' => 'court_cases_satisfied_edit',
                    'source' => array(
                        array('value' => 1, 'text' => 'Да'),
                        array('value' => 0, 'text' => 'Нет')
                    ),
                    'mode' => 'inline',
                    //'pk' => 'cid',
                    'empty_text' => '',
                )),
            ))

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\View\VWCourtCase';
    }

    /**
     * {@inheritdoc}
     */
    public function changeEntityManager($em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dscourtcases_datatable';
    }
    /**
     * Returns the columns which are to be displayed in a pdf.
     *
     * @return array
     */
    private function getPdfColumns()
    {
            return array(
                '0', '1', '2','3'
            );

    }
}
