<?php

namespace ReportsBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Filter\Select2Filter;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class DSCompanyListDatatable
 *
 * @package ReportsBundle\Datatables
 */
class DSCompanyListDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true,
            'language' => 'ru'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
        'classes' => Style::BOOTSTRAP_3_STYLE,
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'dom' => 'Bfrtip',
            'length_menu' => [
                [ 10, 25, 50, -1 ],
                [ '10 строк', '25 строк', '50 строк', 'Показать все' ]
            ]
        ));

        $this->features->set(array(
            'processing' => true,
            'length_change' => true
        ));

        $this->extensions->set(array(
            //'responsive' => true,
            'buttons' => array(
                'create_buttons' => array(
                    [
                        'extend' => 'excelHtml5',
                        'text' => 'Excel',
                        'button_options'=>[
                            'exportOptions' =>[
                                'columns'=>':visible'//$this->getPdfColumns()
                            ]
                        ]
                    ],

                   ['extend' => 'pageLength']

                ),
            ),
            'responsive' => array(
                'details' => array(
                    'display' => array(
                        'template' => ':extension:display.js.twig',
                    ),
                    'renderer' => array(
                        'template' => ':extension:renderer.js.twig',
                    ),
                ),
            ),
        ));

        $this->callbacks->set(array(
            'row_callback' => array(
                'template' => ':bankruptcy:row_callback.js.twig',
                'vars' => array('route' => '/company/view/'),
            )
        ));

        $this->columnBuilder
            ->add('id', Column::class, array(
                'title' => 'ИД',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
                ))
            ->add('name', Column::class, array(
                'title' => 'Наименование',
                'width' => '100%',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
            ))
            ->add('inn', Column::class, array(
                'title' => 'ИНН',
                'filter' => array(TextFilter::class, ['classes' => 'form-control',])
                ))
            ->add('level.id', Column::class, array(
                'title' => 'Уровень ответственности КФ ОДО',
                'editable' => array(SelectEditable::class,
                    array(
                        'editable_if' => function ($row) {
                      /*      if ($this->getUser()) {
                                if ($row['createdBy']['id'] == $this->getUser()->getId() or true === $this->isAdmin()) {
                                    return true;
                                };
                            }
                            return false;*/
                            return true;
                        },
                        'url' => 'company_field_level_edit',
                        'source' => [
                            ['value' => 1, 'text' => '1'],
                            ['value' => 2, 'text' => '2'],
                            ['value' => 3, 'text' => '3'],
                            ['value' => 4, 'text' => '4'],
                            ['value' => 5, 'text' => '5'],
                        ],
                        'mode' => 'inline',
                        //'empty_text' => 'Уровень',
                    ),
                ),
                'filter' => array(Select2Filter::class, array(
                    'classes' => 'form-control',
                    'search_type' => 'eq',
                    'multiple' => true,
                    'select_options' => array(
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',

                    ),

                )),
            ))
            ->add('levelVV.id', Column::class, array(
                    'title' => 'Уровень ответственности КФ ВВ',
                    'editable' => array(SelectEditable::class,
                        array(
                            'editable_if' => function ($row) {
                                /*      if ($this->getUser()) {
                                          if ($row['createdBy']['id'] == $this->getUser()->getId() or true === $this->isAdmin()) {
                                              return true;
                                          };
                                      }
                                      return false;*/
                                return true;
                            },
                            'url' => 'company_field_vv_level_edit',
                            'source' => [
                                ['value' => 1, 'text' => '1'],
                                ['value' => 2, 'text' => '2'],
                                ['value' => 3, 'text' => '3'],
                                ['value' => 4, 'text' => '4'],
                                ['value' => 5, 'text' => '5'],
                            ],
                            'mode' => 'inline',
                            //'empty_text' => 'Уровень',
                        ),
                    ),
                    'filter' => array(Select2Filter::class, array(
                        'classes' => 'form-control',
                        'search_type' => 'eq',
                        'multiple' => true,
                        'select_options' => array(
                            '1' => '1',
                            '2' => '2',
                            '3' => '3',
                            '4' => '4',
                            '5' => '5',

                        ),

                    )),
                )
            );

    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'ReportsBundle\Entity\DSCompanyList';
    }

    /**
     * {@inheritdoc}
     */
    public function changeEntityManager($em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dscompanylist_datatable';
    }
    /**
     * Returns the columns which are to be displayed in a pdf.
     *
     * @return array
     */
    private function getPdfColumns()
    {

            return array(
                '0', // id column
                '1', // title column
                '2', // visible column
            );

    }
}
