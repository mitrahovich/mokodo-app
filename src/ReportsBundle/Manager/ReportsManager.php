<?php
namespace ReportsBundle\Manager;

use AppBundle\Entity\DataSource;
use AppBundle\Filter\Filter;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;

/**
 * Class ReportsManager
 * @package ReportsBundle\Manager
 */
class ReportsManager
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @return ObjectManager
     */
    public function getEm(): ObjectManager
    {
        return $this->em;
    }

    /**
     * @param ObjectManager $em
     */
    public function setEm(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return DataSource|null|object
     */
    public function getCurrentDS($request)
    {
        $session = $request->getSession();
        return $this->getEm()->find(DataSource::class, $session->get('ds_id'));
    }

}