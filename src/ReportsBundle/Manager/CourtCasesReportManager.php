<?php
namespace ReportsBundle\Manager;

use AppBundle\Entity\DataSource;
use AppBundle\Filter\Filter;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;

/**
 * Class CourtCasesReportManager
 * @package ReportsBundle\Manager
 */
class CourtCasesReportManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @return EntityManager
     */
    public function getEm(): EntityManager
    {
        return $this->em;
    }

    /**
     * @param EntityManager $em
     */
    public function setEm(EntityManager $em)
    {
        $this->em = $em;
    }



}