<?php
namespace AppBundle\Form\Type;

use AppBundle\Entity\DataSource;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\File;

class DataSourceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('src', FileType::class, [
            'label' => 'Файл',
            'required' => true,
            'attr' => [
                'class' => 'file-inputs primary',
                'title' => 'Выбрать',
                'data-filename-placement' => 'inside'
            ],
            'constraints' => [
                new File([
                    'mimeTypes' => ['text/csv', 'text/plain']
                ])
            ]
        ])->add('title', TextType::class, [
            'label' => 'Заголовок'
        ])->add('submit', SubmitType::class, [
            'label' => 'Загрузить',
            'attr' => ['class' => 'btn-info']
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => DataSource::class,
        ));
    }
}