<?php
namespace AppBundle\Event;

use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class EditFlagCompanyEvent extends Event
{
    /** @var \ReportsBundle\Entity\DSCompanyList
     */
    private $company;

    /** @var User*/
    private $user;

    /** @var boolean */
    private $checked;

    /**
     * EditFlagCompanyEvent constructor.
     * @param \ReportsBundle\Entity\DSCompanyList $company
     * @param User $user
     * @param bool $checked
     */
    public function __construct(\ReportsBundle\Entity\DSCompanyList $company, User $user, $checked)
    {
        $this->company = $company;
        $this->user = $user;
        $this->checked = $checked;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return \ReportsBundle\Entity\DSCompanyList
     */
    public function getCompany(): \ReportsBundle\Entity\DSCompanyList
    {
        return $this->company;
    }

    /**
     * @return bool
     */
    public function isChecked(): bool
    {
        return $this->checked;
    }


}