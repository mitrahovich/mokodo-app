<?php
namespace AppBundle\Event;

use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class EditFlagCourtCaseEvent extends Event
{
    /** @var \AppBundle\Entity\DSCourtCase
     */
    private $courtCase;

    /** @var User*/
    private $user;

    /** @var boolean */
    private $checked;

    /**
     * EditFlagCourtCaseEvent constructor.
     * @param \AppBundle\Entity\DSCourtCase $courtCase
     * @param User $user
     * @param bool $checked
     */
    public function __construct(\AppBundle\Entity\DSCourtCase $courtCase, User $user, $checked)
    {
        $this->courtCase = $courtCase;
        $this->user = $user;
        $this->checked = $checked;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return \AppBundle\Entity\DSCourtCase
     */
    public function getCourtCase(): \AppBundle\Entity\DSCourtCase
    {
        return $this->courtCase;
    }

    /**
     * @return bool
     */
    public function isChecked(): bool
    {
        return $this->checked;
    }


}