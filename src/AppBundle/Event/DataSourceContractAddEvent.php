<?php
namespace AppBundle\Event;

use AppBundle\Entity\DSContract;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\Event;

class DataSourceContractAddEvent extends Event
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var DSContract
     */
    private $contract;

    /**
     * DataSourceContractUpdateEvent constructor.
     * @param EntityManager $entityManager
     * @param DSContract $contract
     */
    public function __construct(EntityManager $entityManager, DSContract $contract)
    {
        $this->entityManager = $entityManager;
        $this->contract = $contract;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @return DSContract
     */
    public function getContract(): DSContract
    {
        return $this->contract;
    }

}