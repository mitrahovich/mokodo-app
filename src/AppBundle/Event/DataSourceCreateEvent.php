<?php
namespace AppBundle\Event;

use AppBundle\Entity\DataSource;
use Symfony\Component\EventDispatcher\Event;

class DataSourceCreateEvent extends Event
{
    /** @var DataSource*/
    private $ds;

    /**
     * DataSourceCreateEvent constructor.
     * @param DataSource $ds
     */
    public function __construct(DataSource $ds)
    {
        $this->ds = $ds;
    }

    /**
     * @return DataSource
     */
    public function getDs(): DataSource
    {
        return $this->ds;
    }

}