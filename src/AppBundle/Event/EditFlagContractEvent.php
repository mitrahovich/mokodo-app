<?php
namespace AppBundle\Event;

use AppBundle\Entity\DSContract;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\Event;

class EditFlagContractEvent extends Event
{
    /** @var DSContract*/
    private $contract;

    /** @var User*/
    private $user;

    /** @var boolean */
    private $checked;
    /**
     * @var EntityManager
     */
    private $liteEm;

    /**
     * EditFlagContractEvent constructor.
     * @param EntityManager $liteEm
     * @param DSContract $contract
     * @param User $user
     * @param bool $checked
     */
    public function __construct(EntityManager $liteEm, DSContract $contract, User $user, $checked)
    {
        $this->contract = $contract;
        $this->user = $user;
        $this->checked = $checked;
        $this->liteEm = $liteEm;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return DSContract
     */
    public function getContract(): DSContract
    {
        return $this->contract;
    }

    /**
     * @return bool
     */
    public function isChecked(): bool
    {
        return $this->checked;
    }

    /**
     * @return EntityManager
     */
    public function getLiteEm(): EntityManager
    {
        return $this->liteEm;
    }


}