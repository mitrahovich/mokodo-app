<?php
namespace AppBundle\Model;

use AppBundle\Exception\CompanyTypeResolverException;
use ITSymfony\Bundle\SeldonBundle\Model\CompanyTypeResolverModel;

class CompanyTypeResolver implements CompanyTypeResolverModel
{
    /**
     * @param string $inn
     * @return int
     * @throws CompanyTypeResolverException
     */
    public function getType($inn): int
    {
        if (strlen($inn) === 10) {

            return CompanyTypeResolverModel::UL_TYPE;

        }

        if (strlen($inn) === 12) {

            return CompanyTypeResolverModel::IP_TYPE;

        }

        throw new CompanyTypeResolverException('Тип организации не определен по ИНН = ' . $inn);
    }

}