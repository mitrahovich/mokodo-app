<?php
namespace AppBundle\Manager;

use AppBundle\Entity\DataSource;

use AppBundle\Event\DataSourceCreateEvent;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class DataSourceManager
 * @package AppBundle\Manager
 */
class DataSourceManager
{
    const DEFFAULT_LEVEL = 0;

    private $levels = [
            '1' => true,
            '2' => true,
            '3' => true,
            '4' => true,
            '5' => true,
         ];

    private $vv_levels = [
        '1' => true,
        '2' => true,
        '3' => true,
        '4' => true,
        '5' => true,
    ];

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * DataSourceManager constructor.
     * @param EntityManager $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
    }


    /**
     * @return DataSource[]|array
     */
    public function getAllDsList()
    {
        return $this->em->getRepository(DataSource::class)->findAll();
    }

    /**
     * @param DataSource $ds
     * @return array
     */
    public function getDataFromCSV(DataSource $ds)
    {
        try {
            $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
            return $serializer->decode(file_get_contents($ds->getSrc()->getPathname()), 'csv');
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param $fileName
     * @param array $data
     * @return mixed
     */
    public function createSqliteDS($fileName, array $data)
    {


        $query = <<<INSTALL
CREATE TABLE "main"."dict_level" (
  "id" INTEGER PRIMARY KEY NOT NULL UNIQUE,
  "deposit" DECIMAL(12, 0),
  "commitment" DECIMAL(16, 0)
);

CREATE TABLE "main"."dict_vv_level" (
  "id" INTEGER PRIMARY KEY NOT NULL UNIQUE,
  "deposit" DECIMAL(12, 0),
  "commitment" DECIMAL(16, 0)
);

INSERT INTO "main"."dict_level" VALUES 
(0, 0, 0),
(1, 200000, 60000000),
(2, 2500000, 500000000),       
(3, 4500000, 3000000000),       
(4, 7500000, 10000000000),       
(5, 25000000, 9999999999999999);    

CREATE TABLE "main"."dict_ds_history_operation" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
  "title" TEXT NOT NULL,
  "code" VARCHAR(64) NOT NULL UNIQUE
);

INSERT INTO "main"."dict_ds_history_operation"(title, code) VALUES 
('Добавление контракта', 'add_contract');

INSERT INTO "main"."dict_vv_level" VALUES 
(0, 0, 0),
(1, 100000, 60000000),
(2, 500000, 500000000),       
(3, 1500000, 3000000000),       
(4, 2000000, 10000000000),       
(5, 5000000, 9999999999999999);   

CREATE TABLE "main"."company_list" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
  "inn" VARCHAR(20) NOT NULL UNIQUE,
  "dict_level_id" INTEGER,
  "dict_vv_level_id" INTEGER,
  "name" TEXT,
  "created" DATETIME DEFAULT CURRENT_TIMESTAMP,
  "updated" DATETIME,
  "sync" BOOL NOT NULL  DEFAULT false,
  FOREIGN KEY(dict_level_id) REFERENCES dict_level(id),
  FOREIGN KEY(dict_vv_level_id) REFERENCES dict_vv_level(id)
);

CREATE TABLE "main"."company_card" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
  "company_list_id" INTEGER NOT NULL,
  "data" TEXT,
  "name" TEXT,
  "ogrn" TEXT,
  "status" TEXT,
  "address" TEXT,
  "region_code" INTEGER,
  "companies_at_this_address" INTEGER,
  "capital" DECIMAL(16,2),
  "trustworthiness_index" INTEGER,
  FOREIGN KEY(company_list_id) REFERENCES company_list(id)
);

CREATE TABLE "main"."company_phones" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
  "company_card_id" INTEGER NOT NULL,
  "phone" TEXT,
  FOREIGN KEY(company_card_id) REFERENCES company_card(id)
);

CREATE TABLE "main"."company_emails" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
  "company_card_id" INTEGER NOT NULL,
  "email" TEXT,
  FOREIGN KEY(company_card_id) REFERENCES company_card(id)
);

CREATE TABLE "main"."bankruptcy" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
  "company_list_id" INTEGER NOT NULL,
  "data" TEXT,
  "publish_date" DATETIME,
  "number" TEXT,
  "type" TEXT,
  "case_number" TEXT,
  "text" TEXT,
  FOREIGN KEY(company_list_id) REFERENCES company_list(id)
);

CREATE TABLE main.contracts (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
  "company_list_id" INTEGER NOT NULL,
  "data" TEXT,
  "contract_number" TEXT,
  "subject" TEXT,
  "law_number" TEXT,
  "status" TEXT,
  "status_code" INTEGER,
  "term_date" DATETIME,
  "contract_date" DATETIME,
  "cancel_date" DATETIME,
  "exec_date" DATETIME,
  "sum" DECIMAL(12,2),
  FOREIGN KEY(company_list_id) REFERENCES company_list(id)
);

CREATE TABLE main.court_cases (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
  "company_list_id" INTEGER NOT NULL,
  "data" TEXT,
  "case_number" TEXT,
  "case_date" DATETIME,
  "case_type" TEXT,
  "sum" DECIMAL(12,2),
  "is_active" BOOLEAN,
  "current_instance" TEXT,
  "instance_date" DATETIME,
  FOREIGN KEY(company_list_id) REFERENCES company_list(id)
);

CREATE TABLE main.court_case_sides (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
  "court_case_id" INTEGER NOT NULL,
  "data" TEXT,
  "name" TEXT,
  "type" INTEGER,
  "ogrn" TEXT,
  "inn" TEXT,
  FOREIGN KEY(court_case_id) REFERENCES court_cases(id)
);

CREATE VIEW "vw_court_cases" AS 
  SELECT 
    cl.id as 'id',
    cl.name as 'name', 
    cl.inn as 'inn',
    count(cc.id) as 'clericalwork_count',
    (SELECT max(c.case_date)  FROM court_cases c WHERE c.is_active=1 ) as 'clericalwork_last_date' 
  FROM 
    company_list cl 
  JOIN court_cases cc ON cc.company_list_id=cl.id 
  GROUP BY cc.company_list_id;

CREATE VIEW "vw_execution_contract" AS  
  SELECT  
    cl.id as 'id', 
    cl.name as 'name', 
    cl.inn as 'inn', 
    count(cc.id) as 'failed_contract_count', 
    max(cc.term_date) as 'failed_contract_last_date'  
  FROM company_list cl  
  JOIN contracts cc ON cc.company_list_id=cl.id 
  WHERE  
    cc.status_code=2 
    AND cc.term_date<CURRENT_TIMESTAMP 
  GROUP BY cc.company_list_id;

CREATE VIEW "vw_bankruptcy" AS 
  SELECT  
    cl.id as 'id', 
    cl.name as 'name',  
    cl.inn as 'inn', 
    ca.status as 'procedure',
    br.case_number  as 'work_number', 
    br.publish_date as 'work_date'  
  FROM company_list cl  
  JOIN bankruptcy br ON br.company_list_id=cl.id 
  JOIN company_card ca ON ca.company_list_id=cl.id 
  GROUP BY br.company_list_id;

CREATE VIEW "vw_kf_odo" AS
  SELECT  
    cl.id as 'id',  
    cl.name as 'name', 
    cl.inn as 'inn',  
    cl.dict_level_id as 'level',
    dl.commitment  as  'limit_summ', 
    count(cc.id) as  'contract_counts', -- кол-во действующих контрактов (выставлена отметка соответствия)
    coalesce(sum(cc.sum) ,'0') as 'contract_summ',  -- сумма дейсвующих котнтрактов (выставлена отметка соответствия)
    coalesce(ce.excess, '0') as 'excess' 
  FROM company_list cl
    LEFT JOIN contracts cc ON cc.company_list_id = cl.id AND EXISTS(SELECT 1 FROM contract_conformity _inner WHERE _inner.contract_id = cc.id AND _inner.conformity = 1)
    LEFT JOIN company_card ca ON ca.company_list_id = cl.id
    JOIN dict_level dl ON dl.id = cl.dict_level_id
    LEFT JOIN company_excess_kfodo ce ON ce.company_id = cl.id
  GROUP BY cl.id;
  
  
CREATE VIEW "wv_dashboard_report" AS
 SELECT  
    cl.id as 'id',  
    cl.name as 'name', 
    cl.inn as 'inn',  
    cl.dict_level_id as 'dict_level_id',
    cl.dict_vv_level_id as 'dict_vv_level_id',
    coalesce(max(cc.sum) ,'0') as 'contract_max_summ',  
    dl.commitment  as  'limit_summ_odo', 
   dl_vv.commitment  as  'limit_summ_vv', 
     count(crc.id)  as 'court_cases_count',
    count(cc.id) as  'contract_count', -- кол-во действующих контрактов (выставлена отметка соответствия)
    coalesce(sum(cc.sum) ,'0') as 'contract_summ'  -- сумма дейсвующих котнтрактов (выставлена отметка соответствия)    
  FROM company_list cl
    LEFT JOIN contracts cc ON cc.company_list_id = cl.id AND EXISTS(SELECT 1 FROM contract_conformity _inner WHERE _inner.contract_id = cc.id AND _inner.conformity = 1)
    LEFT JOIN court_cases crc ON crc.company_list_id = cl.id
    LEFT JOIN company_card ca ON ca.company_list_id = cl.id
    LEFT JOIN dict_level dl ON dl.id = cl.dict_level_id
   LEFT  JOIN dict_vv_level dl_vv ON dl_vv.id = cl.dict_vv_level_id
  GROUP BY cl.id;
  
  
   
  
  
CREATE VIEW "vw_kf_vv" AS 
    SELECT  
    cl.id as 'id',  
    cl.name as 'name', 
    cl.inn as 'inn',  
    cl.dict_level_id as 'level',
    dl.commitment  as  'limit_summ', 
    coalesce(max(cc.sum) ,'0') as 'contract_max_summ',  
    coalesce(ce.excess, '0') as 'excess' 
  FROM company_list cl
    LEFT JOIN contracts cc ON cc.company_list_id = cl.id AND EXISTS(SELECT 1 FROM contract_conformity _inner WHERE _inner.contract_id = cc.id AND _inner.conformity = 1)
    LEFT JOIN company_card ca ON ca.company_list_id = cl.id
    JOIN dict_vv_level dl ON dl.id = cl.dict_vv_level_id
    LEFT JOIN company_excess_kfvv ce ON ce.company_id = cl.id
  GROUP BY cl.id;
  
CREATE VIEW "vw_contracts" AS 
  SELECT 
    cc.id as 'id',
    cl.inn as 'inn',
    cl.name as 'name',
    ca.company_list_id,
    cc.contract_number as 'id_contract',
    0 as 'okdp', 
    cc.subject as 'desc', 
    cc.contract_date as 'contract_date',
    cc.term_date as 'plan_date', 
    cc.exec_date as 'fact_date', 
    dl.id||'-'||dl.commitment  as 'level',
    cc.sum as 'contract_summ', 
    ( SELECT sum(c.sum) FROM contracts c WHERE c.law_number='223-ФЗ' AND c.id=cc.id)as 'kfm_summ', 
    coalesce(co.conformity, '0') as 'satisfied' 
  FROM contracts cc 
  JOIN company_list cl ON cl.id= cc.company_list_id
  JOIN company_card ca ON ca.company_list_id=cl.id 
  JOIN dict_level dl ON dl.id=cl.dict_level_id
  LEFT JOIN contract_conformity co ON co.contract_id=cc.id;
 
CREATE VIEW "vw_card_court_cases" AS 
  SELECT 
      ccs.name as 'defendant', 
      ccs.inn as 'defendant_inn', 
      (SELECT name FROM  court_case_sides cs WHERE cs.court_case_id=cs.id) as 'claimant',
      cc.id, 
      cc.company_list_id,
      cc.case_number,
      cc.case_date,
      cc.case_type,
      cc.sum,
      cc.is_active,
      cc.instance_date,
      coalesce(ccc.conformity, '0') as 'satisfied' 
  FROM court_cases cc 
  JOIN court_case_sides ccs ON ccs.court_case_id=cc.id AND ccs.type=1
  LEFT JOIN court_cases_conformity ccc ON ccc.court_cases_id=cc.id;

CREATE TABLE "company_excess_kfodo" (
  "company_id" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , 
  "excess" INTEGER
  );
  
CREATE TABLE "company_excess_kfvv" (
  "company_id" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , 
  "excess" INTEGER
  );
  
CREATE  TABLE "court_cases_conformity" (
  "court_cases_id" INTEGER PRIMARY KEY  NOT NULL , 
  "conformity" INTEGER
  );
  
CREATE TABLE "contract_conformity" (
  "contract_id" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , 
  "conformity" INTEGER
  );

CREATE TABLE "main"."data_source_history" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
  "contract_id" INTEGER NOT NULL,
  "ds_history_operation_id" INTEGER NOT NULL,
  "created" DATETIME DEFAULT CURRENT_TIMESTAMP,
  "readed" DATETIME,
  FOREIGN KEY(contract_id) REFERENCES contracts(id),
  FOREIGN KEY(ds_history_operation_id) REFERENCES dict_ds_history_operation(id)
);

INSTALL;


        $sql3_db = new \SQLite3($fileName);
        $sql3_db->exec($query);

        foreach ($data as $itemData)
        {
            try {
                $row = array_values($itemData);
                $level = trim($row[1]);
                if(array_key_exists($level, $this->levels) === false) {
                    $level = self::DEFFAULT_LEVEL;
                }
                $levelVV = trim($row[2]);
                if(array_key_exists($levelVV, $this->vv_levels) === false) {
                    $levelVV = self::DEFFAULT_LEVEL;
                }
                $sql3_db->exec('INSERT INTO company_list(inn, dict_level_id, dict_vv_level_id) VALUES ("' . trim($row[0]) . '","' . $level . '","' . $levelVV . '")');
            } catch (\Exception $e) {
                //TODO ADD LOGGING
                syslog(LOG_INFO, $e->getMessage());
                continue;
            }
        }
        $sql3_db->close();

        return $fileName;
    }

    /**
     * @param DataSource $ds
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function save(DataSource $ds): void
    {
        $this->em->persist($ds);
    }

    /**
     * @param DataSource $ds
     */
    public function notifyAboutSave(DataSource $ds): void
    {
        $this->dispatcher->dispatch('ds_created', new DataSourceCreateEvent($ds));
    }


    /**
     * @param DataSource $dataSource
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function remove(DataSource $dataSource): void
    {
        $this->em->remove($dataSource);
    }

    public function flush(): void
    {
        $this->em->flush();
    }

    /**
     * @param string $path
     * @param ObjectManager $em
     * @return EntityManager
     */
    public static function createEntityManagerSQLite($path, ObjectManager $em)
    {
        $conn = array(
            'driver' => 'pdo_sqlite',
            'path' => $path,
            'charset' => 'UTF8'
        );

        return \Doctrine\ORM\EntityManager::create(
            $conn,
            $em->getConfiguration(),
            $em->getEventManager()
        );
    }

}