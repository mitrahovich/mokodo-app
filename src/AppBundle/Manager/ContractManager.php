<?php
namespace AppBundle\Manager;

use AppBundle\Entity\DSCompanyList;
use AppBundle\Entity\DSContract;
use ITSymfony\Bundle\SeldonBundle\Api\SeldonApi;
use Symfony\Bridge\Monolog\Logger;

class ContractManager
{
    /**
     * @var SeldonApi
     */
    private $api;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * ContractManager constructor.
     * @param SeldonApi $api
     * @param Logger $logger
     */
    public function __construct(SeldonApi $api, Logger $logger)
    {
        $this->api = $api;
        $this->logger = $logger;
    }

    /**
     * @param DSCompanyList $DSCompanyList
     * @return array
     */
    public function loadContracts(DSCompanyList $DSCompanyList): array
    {
        $response = null;

        try {

            $response = $this->api->getContracts([
                'inn' => $DSCompanyList->getInn(),
                'type' => 0
            ]);

            $this->api->logout();

            return json_decode($response, true);

        } catch (\Exception $exception) {

            $this->logger->error($exception->getMessage(), [
                'response' => $response, 'trace' => $exception->getTraceAsString()
            ]);

            return [];
        }

    }

    /**
     * @param array $contractData
     * @param DSCompanyList $DSCompanyList
     * @return DSContract
     */
    public function createNewContract(array $contractData, DSCompanyList $DSCompanyList): DSContract
    {
        $termDate = $contractData['termDate'] ? new \DateTime($contractData['termDate']) : null;
        $contractDate = $contractData['contractDate'] ? new \DateTime($contractData['contractDate']) : null;
        $cancelDate = $contractData['cancelDate'] ? new \DateTime($contractData['cancelDate']) : null;
        $execDate = $contractData['execDate'] ? new \DateTime($contractData['execDate']) : null;

        $contract = (new DSContract())
            ->setData($contractData)
            ->setCompanyList($DSCompanyList)
            ->setContractNumber($contractData['contractNo'])
            ->setSubject($contractData['subject'])
            ->setLawNumber($contractData['lawNo'])
            ->setStatus($contractData['status']['name'])
            ->setStatusCode((int)$contractData['status']['code'])
            ->setTermDate($termDate)
            ->setContractDate($contractDate)
            ->setCancelDate($cancelDate)
            ->setExecDate($execDate)
            ->setSum($contractData['sum']);

        return $contract;
    }

}