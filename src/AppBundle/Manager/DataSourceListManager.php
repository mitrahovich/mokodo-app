<?php
namespace AppBundle\Manager;

use AppBundle\Entity\DataSource;
use AppBundle\Filter\Filter;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;

/**
 * Class DataSourceListManager
 * @package AppBundle\Manager
 */
class DataSourceListManager
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var Paginator
     */
    private $paginator;

    /** @var CollectionFormatter*/
    private $formatter;

    /**
     * @var array
     */
    private $listFilters = [
        's.id' => 'eq',
        's.title' => 'eq',
        's.src' => 'eq',
        's.created' => 'eq',
    ];

    /**
     * @param EntityManager $em
     * @param Paginator $paginator
     */
    public function __construct(
        EntityManager $em,
        Paginator $paginator
    ) {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function getListFilters()
    {
        $filters = $this->listFilters;
        return $filters;
    }

    public function getPaginate($page, $limit, Filter $filter) : PaginationInterface
    {
        $qb = $this->em->createQueryBuilder()
            ->select('s')
            ->from('AppBundle\Entity\DataSource','s')
        ;

        $filter->addFiltersToQB($qb);

        return $this->paginator->paginate($qb, $page, $limit,
            ['defaultSortFieldName' => 's.id', 'defaultSortDirection' => 'asc']
        );
    }
}