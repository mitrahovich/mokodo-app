<?php
namespace AppBundle\RabbitMq\Consumer;

use AppBundle\Entity\DSCompanyCard;
use AppBundle\Entity\DSCompanyEmail;
use AppBundle\Entity\DSCompanyPhone;
use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use ITSymfony\Bundle\SeldonBundle\Api\SeldonApi;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\Producer;
use PhpAmqpLib\Message\AMQPMessage;

class LoadCardConsumer implements ConsumerInterface
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var Producer
     */
    private $contractsProducer;
    /**
     * @var Producer
     */
    private $courtCasesProducer;
    /**
     * @var Producer
     */
    private $bankruptcyProducer;
    /**
     * @var SeldonApi
     */
    private $api;

    public function __construct(
        EntityManager $entityManager, Producer $contractsProducer,
        Producer $courtCasesProducer, Producer $bankruptcyProducer, SeldonApi $api
    )
    {
        $this->entityManager = $entityManager;
        $this->contractsProducer = $contractsProducer;
        $this->courtCasesProducer = $courtCasesProducer;
        $this->bankruptcyProducer = $bankruptcyProducer;
        $this->api = $api;
    }

    public function execute(AMQPMessage $msg)
    {

        sleep(1);
        //TODO ADD LOGGING
        $msgBody = $msg->getBody();
        $data = unserialize($msgBody, []);

        try {
            $dataSource = $this->entityManager->find('AppBundle:DataSource', $data['ds_id']);
        } catch (ORMException $exception) {
            dump($exception->getMessage());
            return true;
        }

        if (null === $dataSource) {
            dump($data['ds_id'] . ' DATA SOURCE IS NULL');
            return true;
        }

        $em = DataSourceManager::createEntityManagerSQLite($dataSource->getFileDb(), $this->entityManager);

        $company = $em->find('AppBundle:DSCompanyList', $data['ds_company_id']);

        if (null === $company) {
            dump($data['ds_company_id'] . ' IS NULL');
            return true;
        }

        dump($company->getInn());

        try {
            $response = $this->api->getDataByInn(trim($company->getInn()));
            $this->api->logout();
            $response = json_decode($response, true);

            dump($response);

            if (array_key_exists('company_card', $response) && is_array($response['company_card'])) {

                $card = $response['company_card'] ?? [];

                $companyCard = (new DSCompanyCard())
                    ->setData($response)
                    ->setCompanyList($company)
                    ->setName($card['basic']['fullName'])
                    ->setOgrn($card['basic']['ogrn'])
                    ->setStatus($card['basic']['status']['name'])
                    ->setAddress($card['address'])
                    ->setRegionCode($card['region_code'])
                    ->setCompaniesAtThisAddress($card['companiesAtThisAddress'])
                    ->setCapital($card['capital'])
                    ->setTrustworthinessIndex($card['trustworthinessIndex']['value']);

                $em->persist($companyCard);

                $company->setName($card['basic']['fullName']);

                $phoneList = $card['phone_list'] ?? [];

                foreach ($phoneList as $phoneData) {

                    $phone = (new DSCompanyPhone())
                        ->setCompanyCard($companyCard)
                        ->setPhone($phoneData);

                    $em->persist($phone);
                }

                $emailList = $card['email_list'] ?? [];

                foreach ($emailList as $emailData) {

                    $email = (new DSCompanyEmail())
                        ->setCompanyCard($companyCard)
                        ->setEmail($emailData);

                    $em->persist($email);
                }

            } elseif (array_key_exists('basic', $response) && is_array($response['basic'])) {

                $card = $response['basic'];

                $companyCard = (new DSCompanyCard())
                    ->setData($response)
                    ->setCompanyList($company)
                    ->setName($card['person']['fullName'])
                    ->setOgrn($card['ogrnip'])
                    ->setStatus($card['status']['name'])
                    ->setRegionCode($card['region']['code']);

                $em->persist($companyCard);

                $company->setName($card['person']['fullName']);
            }

            $em->flush();


        } catch (\Exception $exception) {
            dump($exception->getMessage());
            return true;
        }

        $this->contractsProducer->publish($msgBody);
        $this->courtCasesProducer->publish($msgBody);
        $this->bankruptcyProducer->publish($msgBody);

        return true;

    }

}