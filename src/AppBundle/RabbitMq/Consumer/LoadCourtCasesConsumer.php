<?php
namespace AppBundle\RabbitMq\Consumer;

use AppBundle\Entity\DSCourtCase;
use AppBundle\Entity\DSCourtCaseSide;
use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use ITSymfony\Bundle\SeldonBundle\Api\SeldonApi;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class LoadCourtCasesConsumer implements ConsumerInterface
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var SeldonApi
     */
    private $api;

    public function __construct(EntityManager $entityManager, SeldonApi $api)
    {
        $this->entityManager = $entityManager;
        $this->api = $api;
    }

    public function execute(AMQPMessage $msg)
    {
        sleep(1);
        // TODO ADD LOGGING
        $data = unserialize($msg->getBody(), []);

        dump($data);

        try {
            $dataSource = $this->entityManager->find('AppBundle:DataSource', $data['ds_id']);
        } catch (ORMException $exception) {
            dump($exception->getMessage());
            return true;
        }

        if (null === $dataSource) {
            dump('DATASOURCE NOT FOUND');
            return true;
        }

        $em = DataSourceManager::createEntityManagerSQLite($dataSource->getFileDb(), $this->entityManager);

        $company = $em->find('AppBundle:DSCompanyList', $data['ds_company_id']);

        if (null === $company) {
            dump('COMPANY NOT FOUND');
            return true;
        }

        try {
            $response = $this->api->getCourtCases(['inn' => $company->getInn()]);
            $this->api->logout();

            $response = json_decode($response, true);
            dump($response);

            if (array_key_exists('cases_list', $response) && is_array($response['cases_list'])) {

                $casesList = $response['cases_list'] ?? [];

                foreach ($casesList as $case) {
                    $courtCase = (new DSCourtCase())
                        ->setData($case)
                        ->setCompanyList($company)
                        ->setCaseNumber($case['caseNo'])
                        ->setCaseDate(new \DateTime($case['caseDate']))
                        ->setCaseType($case['caseType']['name'])
                        ->setSum($case['sum'])
                        ->setIsActive($case['isActive'])
                        ->setCurrentInstance($case['currentInstance'])
                        ->setInstanceDate(new \DateTime($case['instanceDate']));

                    $em->persist($courtCase);

                    if (array_key_exists('case_sides', $case) && is_array($case['case_sides'])) {
                        $caseSides = $case['case_sides'] ?? [];

                        foreach ($caseSides as $side) {
                            $side = (new DSCourtCaseSide())
                                ->setData($side)
                                ->setCourtCase($courtCase)
                                ->setName($side['name'])
                                ->setType($side['type'])
                                ->setOgrn($side['OGRN'])
                                ->setInn($side['INN']);

                            $em->persist($side);
                        }
                    }
                }
                $em->flush();
            }

        } catch (\Exception $exception) {
            dump($exception->getMessage());
            return true;
        }

        return true;
    }

}