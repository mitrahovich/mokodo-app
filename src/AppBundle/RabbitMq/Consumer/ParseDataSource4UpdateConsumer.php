<?php
namespace AppBundle\RabbitMq\Consumer;

use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class ParseDataSource4UpdateConsumer implements ConsumerInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ProducerInterface
     */
    private $producer;

    /**
     * ParseDataSource4UpdateConsumer constructor.
     * @param EntityManager $entityManager
     * @param ProducerInterface $producer
     */
    public function __construct(EntityManager $entityManager, ProducerInterface $producer)
    {
        $this->entityManager = $entityManager;
        $this->producer = $producer;
    }

    /**
     * Параметры сообщения:
     *  - $data['ds_id'] int AppBundle:DataSource.id
     *
     * @param AMQPMessage $msg
     * @return bool
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function execute(AMQPMessage $msg): bool
    {
        sleep(1);
        // TODO ADD LOGGING
        $data = unserialize($msg->getBody(), []);

        dump($data);

        try {
            $dataSource = $this->entityManager->find('AppBundle:DataSource', $data['ds_id']);
        } catch (ORMException $exception) {
            dump($exception->getMessage());
            return true;
        }

        if (null === $dataSource) {
            dump('DATA SOURCE IS NOT FOUND');
            return true;
        }

        try {
            $em = DataSourceManager::createEntityManagerSQLite($dataSource->getFileDb(), $this->entityManager);
            $companyList = $em->getRepository('AppBundle:DSCompanyList')
                ->findAll();

            dump($companyList);

        } catch (\Exception $exception) {
            dump($exception->getMessage());
            return true;
        }

        foreach ($companyList as $company) {
            try {

                $message = [
                    'ds_id' => $dataSource->getId(),
                    'ds_company_id' => $company->getId()
                ];

                dump($message);

                $this->producer->publish(serialize($message));
            } catch (\Exception $exception) {
                dump($exception->getMessage());
            }
        }

        return true;

    }

}