<?php
namespace AppBundle\RabbitMq\Consumer;

use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\Producer;
use PhpAmqpLib\Message\AMQPMessage;

class ProcessSourceConsumer implements ConsumerInterface
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var Producer
     */
    private $producer;

    /**
     * ProcessSourceConsumer constructor.
     * @param EntityManager $entityManager
     * @param Producer $producer
     */
    public function __construct(EntityManager $entityManager, Producer $producer)
    {
        $this->entityManager = $entityManager;
        $this->producer = $producer;
    }

    /**
     * @param AMQPMessage $msg
     * @return bool
     * @throws ORMInvalidArgumentException
     * @throws ORMException
     */
    public function execute(AMQPMessage $msg): bool
    {
        sleep(1);
        // TODO ADD LOGGING
        $data = unserialize($msg->getBody(), []);

        try {
            $dataSource = $this->entityManager->find('AppBundle:DataSource', $data['ds_id']);
        } catch (ORMException $exception) {
            dump($exception->getMessage());
            return true;
        }

        if (null === $dataSource) {
            dump('DATASOURCE IS NULL');
            return true;
        }

        try {
            $em = DataSourceManager::createEntityManagerSQLite($dataSource->getFileDb(), $this->entityManager);

            $companyList = $em->getRepository('AppBundle:DSCompanyList')
                ->findAll();
        } catch (\Exception $exception) {
            dump($exception->getMessage());
            return true;
        }

        foreach ($companyList as $company) {
            $message = [
                'ds_id' => $data['ds_id'],
                'ds_company_id' => $company->getId()
            ];

            dump($message);
            $this->producer->publish(serialize($message));
        }

        return true;
    }

}