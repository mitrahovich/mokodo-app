<?php
namespace AppBundle\RabbitMq\Consumer;

use AppBundle\Event\DataSourceContractAddEvent;
use AppBundle\Manager\ContractManager;
use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UpdateContractsConsumer implements ConsumerInterface
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var ContractManager
     */
    private $contractManager;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * LoadContractsConsumer constructor.
     * @param EntityManager $entityManager
     * @param ContractManager $contractManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManager $entityManager, ContractManager $contractManager, EventDispatcherInterface $dispatcher)
    {
        $this->entityManager = $entityManager;
        $this->contractManager = $contractManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     *
     * Входные параметры:
     * - $data['ds_id'] int AppBundle:DataSource.id
     * - $data['ds_company_id'] int AppBundle:DSCompanyList.id
     *
     * @param AMQPMessage $msg
     * @return bool
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function execute(AMQPMessage $msg): bool
    {
        sleep(1);
        // TODO ADD LOGGING
        $data = unserialize($msg->getBody(), []);

        dump($data);

        try {
            $dataSource = $this->entityManager->find('AppBundle:DataSource', $data['ds_id']);
        } catch (ORMException $exception) {
            dump($exception->getMessage());
            return true;
        }

        if (null === $dataSource) {
            dump('DATA SOURCE IS NOT FOUND');
            return true;
        }

        $em = DataSourceManager::createEntityManagerSQLite($dataSource->getFileDb(), $this->entityManager);

        $company = $em->find('AppBundle:DSCompanyList', $data['ds_company_id']);

        if (null === $company) {
            dump('COMPANY SOURCE IS NOT FOUND');
            return true;
        }

        try {

            $response = $this->contractManager->loadContracts($company);

            if (array_key_exists('contracts_list', $response) && is_array($response['contracts_list'])) {

                $contractList = $response['contracts_list'] ?? [];

                foreach ($contractList as $contract) {

                    if (!$this->isContractExists($em, $contract['contractNo'])) {

                        $contract = $this->contractManager->createNewContract($contract, $company);
                        dump($contract);
                        $em->persist($contract);
                        $this->dispatcher->dispatch(
                            'ds_contract_add', new DataSourceContractAddEvent($em, $contract)
                        );

                    }

                }

                $em->flush();

            }

        } catch (\Exception $exception) {
            dump($exception->getMessage());
            return true;
        }

        return true;
    }

    /**
     * @param EntityManager $em
     * @param string $contractNumber
     * @return bool
     */
    private function isContractExists(EntityManager $em, $contractNumber): bool
    {
        $contract = $em->getRepository('AppBundle:DSContract')
            ->findOneBy(['contractNumber' => $contractNumber]);

        return null !== $contract;
    }

}