<?php
namespace AppBundle\RabbitMq\Consumer;

use AppBundle\Entity\DSBankruptcy;
use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use ITSymfony\Bundle\SeldonBundle\Api\SeldonApi;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class LoadBankruptcyConsumer implements ConsumerInterface
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var SeldonApi
     */
    private $api;

    public function __construct(EntityManager $entityManager, SeldonApi $api)
    {
        $this->entityManager = $entityManager;
        $this->api = $api;
    }

    public function execute(AMQPMessage $msg)
    {

        sleep(1);
        // TODO LOGGING
        $data = unserialize($msg->getBody(), []);

        dump($data);

        try {
            $dataSource = $this->entityManager->find('AppBundle:DataSource', $data['ds_id']);
        } catch (ORMException $exception) {
            dump($exception->getMessage());
            return true;
        }

        if (null === $dataSource) {
            dump('DATA SOURCE IS NULL');
            return true;
        }

        $em = DataSourceManager::createEntityManagerSQLite($dataSource->getFileDb(), $this->entityManager);

        $company = $em->find('AppBundle:DSCompanyList', $data['ds_company_id']);

        if (null === $company) {
            dump('COMPANY NOT FOUND');
            return true;
        }

        try {
            $response = $this->api->getBankruptcy(['inn' => $company->getInn()]);
            $this->api->logout();
            $response = json_decode($response, true);
            dump($response);


            if (array_key_exists('messages_list', $response) && is_array($response['messages_list'])) {

                $messageList = $response['messages_list'] ?? [];

                foreach ($messageList as $message) {

                    $publishDate = $message['publishDate'] ? new \DateTime($message['publishDate']) : null;

                    $bankruptcy = (new DSBankruptcy())
                        ->setCompanyList($company)
                        ->setData($message)
                        ->setPublishDate($publishDate)
                        ->setNumber($message['number'])
                        ->setType($message['type'])
                        ->setCaseNumber($message['caseNumber'])
                        ->setText($message['text']);

                    $em->persist($bankruptcy);
                }

                $em->flush();
            }

        } catch (\Exception $exception) {
            dump($exception->getMessage());
            return true;
        }

        return true;
    }

}