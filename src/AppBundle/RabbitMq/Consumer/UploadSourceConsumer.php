<?php
namespace AppBundle\RabbitMq\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class UploadSourceConsumer implements ConsumerInterface
{
    public function execute(AMQPMessage $msg)
    {
        sleep(1);
    }

}