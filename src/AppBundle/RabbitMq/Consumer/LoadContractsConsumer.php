<?php
namespace AppBundle\RabbitMq\Consumer;

use AppBundle\Entity\DSContractConformity;
use AppBundle\Manager\ContractManager;
use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class LoadContractsConsumer implements ConsumerInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ContractManager
     */
    private $contractManager;

    /**
     * LoadContractsConsumer constructor.
     * @param EntityManager $entityManager
     * @param ContractManager $contractManager
     */
    public function __construct(EntityManager $entityManager, ContractManager $contractManager)
    {
        $this->entityManager = $entityManager;
        $this->contractManager = $contractManager;
    }

    public function execute(AMQPMessage $msg)
    {
        sleep(1);
        // TODO ADD LOGGING
        $data = unserialize($msg->getBody(), []);

        dump($data);

        try {
            $dataSource = $this->entityManager->find('AppBundle:DataSource', $data['ds_id']);
        } catch (ORMException $exception) {
            dump($exception->getMessage());
            return true;
        }

        if (null === $dataSource) {
            dump('DATA SOURCE IS NOT FOUND');
            return true;
        }

        $em = DataSourceManager::createEntityManagerSQLite($dataSource->getFileDb(), $this->entityManager);

        $company = $em->find('AppBundle:DSCompanyList', $data['ds_company_id']);

        if (null === $company) {
            dump('COMPANY SOURCE IS NOT FOUND');
            return true;
        }

        try {

            $response = $this->contractManager->loadContracts($company);

            if (array_key_exists('contracts_list', $response) && is_array($response['contracts_list'])) {

                $contractList = $response['contracts_list'] ?? [];

                foreach ($contractList as $contract) {

                    $contract = $this->contractManager->createNewContract($contract, $company);

		            $em->persist($contract);

                    $excess = new DSContractConformity();
                    $excess->setContract($contract);
                    $excess->setConformity(true);
                    $em->persist($excess);

                }

                $em->flush();

            }

        } catch (\Exception $exception) {
            dump($exception->getMessage());
            return true;
        }

        return true;
    }

}
