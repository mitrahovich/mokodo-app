<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DataSourceUpdateInitCommand extends ContainerAwareCommand
{
    protected function configure(): void
    {
        $this
            ->setName('data-source:update:init')
            ->setDescription('Инициализация обновления информации в источниках данных')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $producer = $this->getContainer()->get('old_sound_rabbit_mq.parse_data_source4update_producer');

        $dataSources = $em->getRepository('AppBundle:DataSource')
            ->findAll();

        dump($dataSources);

        foreach ($dataSources as $ds) {
            $producer->publish(serialize(['ds_id' => $ds->getId()]));
        }
    }

}
