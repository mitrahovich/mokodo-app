<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        // Дашборд
        $menu->addChild('Главная', ['route' => 'homepage']);

        // Источники данных
        $menu->addChild('Источники данных', ['route' => 'ds']);

        if($this->checkSelectDS()) {
            // Компании текущего источника
            $menu->addChild('Список компаний', ['route' => 'current_company_list']);

            // Отчет КФ ОДО
            $menu->addChild('Отчет КФ ОДО', ['route' => 'report_bkfodo']);

            // Отчет КФ ВВ
            $menu->addChild('Отчет КФ ВВ', ['route' => 'report_kfvv']);

            // Отчет Арбитраж
            $menu->addChild('Отчет Арбитраж', ['route' => 'report_court-cases']);

            // Отчет Контракты
            $menu->addChild('Отчет Контракты', ['route' => 'report_executioncontract']);

            // Отчет БЛ
            $menu->addChild('Отчет БЛ', ['route' => 'report_bankruptcy']);

            // Картотека контрактов
            $menu->addChild('Картотека контрактов', ['route' => 'contracts']);

            // Журнал изменений
            $menu->addChild('Журнал изменений', ['route' => 'log_change']);
        }
        // Журнал операций
        $menu->addChild('Журнал операций', ['route' => 'log_operation']);



        return $menu;
    }

    private function checkSelectDS()
    {
        try {
            $request = $this->container->get('request_stack')->getCurrentRequest();
            $session = $request->getSession();
            if(!$session->get('ds_id')) {
                throw new Exception('ds_id not found');
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}