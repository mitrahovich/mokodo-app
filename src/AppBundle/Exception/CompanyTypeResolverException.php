<?php
namespace AppBundle\Exception;

/**
 * Если не удалось определить тип компании
 */
class CompanyTypeResolverException extends MokodoException {}