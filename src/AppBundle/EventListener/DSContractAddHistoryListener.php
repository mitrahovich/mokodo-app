<?php
namespace AppBundle\EventListener;

use AppBundle\Entity\DSHistory;
use AppBundle\Event\DataSourceContractAddEvent;

class DSContractAddHistoryListener
{
    /**
     * @param DataSourceContractAddEvent $event
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function onContractUpdate(DataSourceContractAddEvent $event): void
    {
        $em = $event->getEntityManager();
        $contract = $event->getContract();

        $operation = $em->getRepository('AppBundle:DSDictHistoryOperation')
            ->findOneBy(['code' => 'add_contract']);

        $dsHistory = new DSHistory($contract, $operation);

        $em->persist($dsHistory);
    }
}