<?php
namespace AppBundle\EventListener;

use AppBundle\Event\DataSourceCreateEvent;
use OldSound\RabbitMqBundle\RabbitMq\Producer;

class DataSourceCreateListener
{
    /**
     * @var Producer
     */
    private $producer;

    /**
     * DataSourceCreateListener constructor.
     * @param Producer $producer
     */
    public function __construct(Producer $producer)
    {
        $this->producer = $producer;
    }

    /**
     * @param DataSourceCreateEvent $event
     */
    public function onDataSourceCreate(DataSourceCreateEvent $event): void
    {
        $ds = $event->getDs();

        $this->producer->publish(serialize(['ds_id' => $ds->getId()]));
    }

}