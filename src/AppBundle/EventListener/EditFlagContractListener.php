<?php
namespace AppBundle\EventListener;

use AppBundle\Entity\DSCompanyExcessKfodo;
use AppBundle\Event\EditFlagContractEvent;
use ReportsBundle\Entity\View\KFODOReport;

class EditFlagContractListener
{
    /**
     * @param EditFlagContractEvent $event
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @return void
     */
    public function onEditFlagContractEvent(EditFlagContractEvent $event): void
    {
        
        $contract = $event->getContract();

        $companyList = $contract->getCompanyList();

        $liteEm = $event->getLiteEm();

        $report = $liteEm->find(KFODOReport::class, $companyList->getId());

        if (null === $report) {
            return;
        }

        $excess =  $liteEm->find(DSCompanyExcessKfodo::class, $companyList->getId());

        if(!($excess instanceof DSCompanyExcessKfodo)) {
            $excess = new DSCompanyExcessKfodo();
        }

        $limitSumm = $report->getLimitSumm();
        $contractSumm = $report->getContractSumm();

        if ($limitSumm > $contractSumm) {
            $excess->setIsExcess(0);
        } else {
            $excess->setIsExcess(1);
        }

        $excess->setCompany($companyList);

        $liteEm->persist($excess);

    }

}