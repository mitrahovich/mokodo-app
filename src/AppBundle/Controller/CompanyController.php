<?php
namespace AppBundle\Controller;

use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/company")
 */
class CompanyController extends Controller
{

    /**
     * @param Request $request
     * @param int $id
     * @return array|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \InvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     * @throws \LogicException
     * @Route("/view/{id}", name="company_view", requirements={"id":"\d+"})
     * @Template()
     */
    public function indexAction(Request $request, int $id)
    {
        try {
            $em = $this->getCurrentLiteEm($request);
        } catch (NotFoundHttpException $exception) {
            return $exception;
        }

        $companyCard = $em->find('AppBundle:DSCompanyCard', $id);

        if (null === $companyCard) {
            return $this->createNotFoundException();
        }

        $contracts = $this->get('card.datatable.contracts');
        $contracts->changeEntityManager($em);
        $contracts->buildDatatable();
        $contracts->getAjax()->setUrl($this->generateUrl('contracts-view',['id' => $id]));

        $courtCases = $this->get('card.datatable.courtcases');
        $courtCases ->changeEntityManager($em);
        $courtCases->buildDatatable();
        $courtCases->getAjax()->setUrl($this->generateUrl('court-cases',['id' => $id]));

        return [
            'companyCard' => $companyCard,
            'contracts' => $contracts,
            'courtCases' => $courtCases
        ];

    }

    /**
     * @param Request $request
     * @return \Exception|JsonResponse|NotFoundHttpException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @Route("/list/all", name="company_list_all")
     */
    public function listAction(Request $request)
    {
        try {
            $em = $this->getCurrentLiteEm($request);
        } catch (NotFoundHttpException $exception) {
            return $exception;
        }

        $collection = $em->getRepository('AppBundle:DSCompanyList')
            ->findAll();

        return new JsonResponse($collection);

    }

    /**
     * @param Request $request
     * @return EntityManager
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    private function getCurrentLiteEm(Request $request): EntityManager
    {
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));
        $currentDs = $reportManager->getCurrentDS($request);

        if (null === $currentDs) {
            throw $this->createNotFoundException();
        }

        return DataSourceManager::createEntityManagerSQLite(
            $currentDs->getFileDb(), $this->getDoctrine()->getManager('dslite')
        );
    }

}