<?php

namespace AppBundle\Controller;

use AppBundle\Filter\Filter;
use AppBundle\Manager\DataSourceListManager;
use AppBundle\Manager\DataSourceManager;
use AppBundle\Manager\DSCompanyManager;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\DataSource;
use AppBundle\Form\Type\DataSourceType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DataSourceController extends Controller
{
    const PAGE_LIMIT = 20;

    /**
     * @Route("/ds", name="ds")
     * @Template()
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \LogicException
     */
    public function indexAction(Request $request)
    {
        $ds = new DataSource();

        $form = $this->createForm(DataSourceType::class, $ds);
        $form->handleRequest($request);

        /** @var DataSourceManager $dsManager */
        $dsManager = $this->get('app.manager.ds_manager');

        /** @var DataSourceListManager $dsListManager */
        $dsListManager = $this->get('app.manager.ds_list_manager');

        /* @var $filter Filter */
        $filter = $this->get('admin.filter')->getFilter($request, $dsListManager->getListFilters());
        $pagination = $dsListManager->getPaginate($request->query->get('page', 1), self::PAGE_LIMIT, $filter);

        if ($form->isSubmitted() && $form->isValid())
        {
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $ds->getSrc();
            $data = $dsManager->getDataFromCSV($ds);

            // Generate a unique name for the file before saving it
            $fileInName = md5(uniqid()).'.'.$file->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                $this->getParameter('ds_upload_directory'),
                $fileInName
            );
            $ds->setSrc($fileInName);
            
            // Generate a unique name for the file
            $fileDbName =  $this->getParameter('ds_directory'). md5(uniqid('', true)).'.db';

            $ds->setFileDb($dsManager->createSqliteDS($fileDbName, $data));
            $ds->setCreated(new \DateTime());
            $dsManager->save($ds);

            $this->getDoctrine()->getManager()->flush();

            $dsManager->notifyAboutSave($ds);

            $this->addFlash('notice','DS_CREATE_SUCCESS');

            return $this->redirect($this->generateUrl('ds'));
        }

        return [
            'form' => $form->createView(),
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }

    /**
     * @Route("/ds/view/{id}", name="ds_view", requirements={
     *  "id": "\d+"
     * })
     * @ParamConverter("id", class="AppBundle:DataSource")
     * @param Request $request
     * @param DataSource $ds
     * @Template()
     * @return array
     * @throws \LogicException
     */
    public function viewAction(Request $request, DataSource $ds)
    {

        /** @var DSCompanyManager $dsManager */
        $dsManager = $this->get('app.manager.ds_company_manager');

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');

        $dsManager->changeEntityManager(
            DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em)
        );

        /* @var $filter Filter */
        $filter = $this->get('admin.filter')->getFilter($request, $dsManager->getListFilters());
        $pagination = $dsManager->getPaginate($request->query->get('page', 1), self::PAGE_LIMIT, $filter);

        return [
            'title' => $ds->getTitle(),
            'pagination' => $pagination,
            'filters' => $filter->getFiltersValue()
        ];
    }

    /**
     * @param DataSource $dataSource
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \LogicException
     * @Route("/ds/remove/{id}", name="ds_remove", requirements={"id": "\d+"})
     * @ParamConverter("id", class="AppBundle:DataSource")
     */
    public function removeAction(DataSource $dataSource)
    {
        $dsManager = $this->get('app.manager.ds_manager');

        $dsManager->remove($dataSource);

        $dsManager->flush();

        $this->addFlash('notice','DS_REMOVE_SUCCESS');

        return $this->redirect($this->generateUrl('ds'));
    }


    /**
     * @Route("/ds/listjson", name="listjson")
     * @Template()
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \LogicException
     * @return JsonResponse
     */
    public function listjsonActionAction(Request $request)
    {
        $dsArr= [];
        /** @var DataSourceManager $dsManager */
        $dsManager = $this->get('app.manager.ds_manager');
        $dsList = $dsManager->getAllDsList();

        $session = $request->getSession();

        foreach($dsList as $dsItem) {
            $dsArr[] = [
                "id" => $dsItem->getId(),
                "text" => $dsItem->getTitle(),
                "selected" => ($dsItem->getId() == $session->get('ds_id'))?true:false
            ];
        }

        return new JsonResponse(['results' => $dsArr]);
    }


    /**
     * @Route("/ds/set/{id}", name="ds_surrent_set", requirements={
     *  "id": "\d+"
     * })
     * @ParamConverter("id", class="AppBundle:DataSource")
     * @param Request $request
     * @param DataSource $ds
     * @return JsonResponse
     * @throws \LogicException
     */
    public function setDsAction(Request $request, DataSource $ds)
    {
        try {
            $session = $request->getSession();
            $session->set('ds_id', $ds->getId());
            $session->set('ds_name', $ds->getTitle());
            return new JsonResponse(['result' => true]);
        } catch (\Exception $e) {
            return new JsonResponse([
                'result' => true,
                'errors' => $e->getMessage()
            ]);
        }
    }
}
