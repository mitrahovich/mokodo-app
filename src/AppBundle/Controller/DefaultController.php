<?php

namespace AppBundle\Controller;

use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use ReportsBundle\Manager\ReportsManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @Template()
     * @throws \ITSymfony\Bundle\SeldonBundle\Exceptions\SeldonException
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \RuntimeException
     */
    public function indexAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();

        /** @var ReportsManager $reportManager */
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));

        try{
            $ds = $reportManager->getCurrentDS($request);
        } catch (\Exception $e) {
            return [
                'ds_name' => 'Источник не выбран',
                'datatable' => false
            ];
        }


        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager('dslite');
        // Get your Datatable ...
        $datatable = $this->get('reports.datatable.dashboard');
        $datatable->changeEntityManager(
            DataSourceManager::createEntityManagerSQLite($ds->getFileDb(), $em)
        );

        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $responseService->getDatatableQueryBuilder();

            return $responseService->getResponse();
        }

        return [
            'ds_name' => $ds->getTitle(),
            'datatable' => $datatable
        ];
    }
}
