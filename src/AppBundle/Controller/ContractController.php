<?php
namespace AppBundle\Controller;

use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ContractController
 * @package AppBundle\Controller
 * @Route("/contract")
 */
class ContractController extends Controller
{

    /**
     * @Route("/view/{id}", name="contract_view", requirements={"id" : "\d+"})
     * @Template()
     * @Method("GET")
     * @param Request $request
     * @param int $id
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @return array
     */
    public function viewAction(Request $request, int $id): array
    {
        $em = $this->getCurrentLiteEm($request);

        $contract = $em->find('AppBundle:DSContract', $id);

        if (null === $contract) {
            throw $this->createNotFoundException();
        }

        return ['contract' => $contract];
    }

    /**
     * @param Request $request
     * @return EntityManager
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    private function getCurrentLiteEm(Request $request): EntityManager
    {
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));
        $currentDs = $reportManager->getCurrentDS($request);

        if (null === $currentDs) {
            throw $this->createNotFoundException();
        }

        return DataSourceManager::createEntityManagerSQLite(
            $currentDs->getFileDb(), $this->getDoctrine()->getManager('dslite')
        );
    }

}