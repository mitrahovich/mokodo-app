<?php
namespace AppBundle\Controller;

use AppBundle\Manager\DataSourceManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SearchController
 * @package AppBundle\Controller
 */
class SearchController extends Controller
{
    /**
     * @Route("/search/company/{inn}", name="search_company_by_inn")
     * @param Request $request
     * @param string $inn
     * @return JsonResponse|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function searchByInnAction(Request $request, string $inn)
    {
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));
        $currentDs = $reportManager->getCurrentDS($request);

        if (null === $currentDs) {
            return $this->createNotFoundException();
        }

        $em = DataSourceManager::createEntityManagerSQLite(
            $currentDs->getFileDb(), $this->getDoctrine()->getManager('dslite')
        );

        $companyList = $em->getRepository('AppBundle:DSCompanyList')
            ->findByInn($inn);

        return new JsonResponse($companyList);
    }

    /**
     * @param Request $request
     * @return \Exception|JsonResponse|NotFoundHttpException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     * @Route("/search/company/list/all", name="company_list_all_for_search")
     */
    public function listAction(Request $request)
    {
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));
        $currentDs = $reportManager->getCurrentDS($request);

        if (null === $currentDs) {
            return $this->createNotFoundException();
        }

        $em = DataSourceManager::createEntityManagerSQLite(
            $currentDs->getFileDb(), $this->getDoctrine()->getManager('dslite')
        );

        $companyList = $em->getRepository('AppBundle:DSCompanyList')
            ->findAll();

        $companyArr = [];
        foreach ($companyList as $companyItem) {
            $companyArr[] = [
                'name' => $companyItem->getInn(),
                'url'  => $this->generateUrl('company_view', ['id' => $companyItem->getId()])
            ];

        }
        return new JsonResponse($companyArr);

    }
}