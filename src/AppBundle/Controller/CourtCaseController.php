<?php
namespace AppBundle\Controller;

use AppBundle\Manager\DataSourceManager;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CourtCaseController
 * @package AppBundle\Controller
 * @Route("/court-case")
 */
class CourtCaseController extends Controller
{

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     * @throws \InvalidArgumentException
     *
     * @Route("/view/{id}", name="court_case_view", requirements={"id" : "\d+"})
     * @Method("GET")
     * @Template()
     */
    public function viewAction(Request $request, int $id): array
    {
        $em = $this->getCurrentLiteEm($request);

        $courtCase = $em->find('AppBundle:DSCourtCase', $id);

        if (null === $courtCase) {
            throw $this->createNotFoundException();
        }

        return ['courtCase' => $courtCase];
    }

    /**
     * @param Request $request
     * @return EntityManager
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    private function getCurrentLiteEm(Request $request): EntityManager
    {
        $reportManager = $this->get('reports.manager');
        $reportManager->setEm($this->getDoctrine()->getManager('default'));
        $currentDs = $reportManager->getCurrentDS($request);

        if (null === $currentDs) {
            throw $this->createNotFoundException();
        }

        return DataSourceManager::createEntityManagerSQLite(
            $currentDs->getFileDb(), $this->getDoctrine()->getManager('dslite')
        );
    }


}