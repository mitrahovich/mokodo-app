<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/log")
 */
class LogController extends Controller
{

    /**
     * @return array
     * @Route("/operation", name="log_operation")
     * @Template()
     */
    public function operationAction(): array
    {
        return [];
    }

    /**
     * @return array
     * @Route("/change", name="log_change")
     * @Template()
     */
    public function changeAction(): array
    {
        return [];
    }
}