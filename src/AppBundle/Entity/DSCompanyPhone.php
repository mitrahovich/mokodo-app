<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="company_phones")
 */
class DSCompanyPhone
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DSCompanyCard
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCompanyCard")
     * @ORM\JoinColumn(name="company_card_id", referencedColumnName="id")
     */
    private $companyCard;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DSCompanyCard
     */
    public function getCompanyCard(): DSCompanyCard
    {
        return $this->companyCard;
    }

    /**
     * @param DSCompanyCard $companyCard
     * @return DSCompanyPhone
     */
    public function setCompanyCard(DSCompanyCard $companyCard): DSCompanyPhone
    {
        $this->companyCard = $companyCard;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return DSCompanyPhone
     */
    public function setPhone(?string $phone): DSCompanyPhone
    {
        $this->phone = $phone;
        return $this;
    }

}