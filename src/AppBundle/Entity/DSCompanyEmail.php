<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="company_emails")
 */
class DSCompanyEmail
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DSCompanyCard
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCompanyCard")
     * @ORM\JoinColumn(name="company_card_id", referencedColumnName="id")
     */
    private $companyCard;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DSCompanyCard
     */
    public function getCompanyCard(): DSCompanyCard
    {
        return $this->companyCard;
    }

    /**
     * @param DSCompanyCard $companyCard
     * @return DSCompanyEmail
     */
    public function setCompanyCard(DSCompanyCard $companyCard): DSCompanyEmail
    {
        $this->companyCard = $companyCard;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return DSCompanyEmail
     */
    public function setEmail(?string $email): DSCompanyEmail
    {
        $this->email = $email;
        return $this;
    }

}