<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="contract_conformity")
 */
class DSContractConformity
{
    /**
     * @ORM\Id
     * @var DSContract
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSContract")
     * @ORM\JoinColumn(name="contract_id", referencedColumnName="id")
     *
     */
    private $contract;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="conformity")
     */
    private $conformity;

    /**
     * @return DSContract
     */
    public function getContract(): DSContract
    {
        return $this->contract;
    }

    /**
     * @param DSContract $contract
     * @return DSContractConformity
     */
    public function setContract(DSContract $contract): DSContractConformity
    {
        $this->contract = $contract;
        return $this;
    }

    /**
     * @return bool
     */
    public function isConformity(): bool
    {
        return $this->conformity;
    }

    /**
     * @param bool $conformity
     * @return DSContractConformity
     */
    public function setConformity(bool $conformity): DSContractConformity
    {
        $this->conformity = $conformity;
        return $this;
    }

    public function __toString()
    {
        return '1';
    }

}