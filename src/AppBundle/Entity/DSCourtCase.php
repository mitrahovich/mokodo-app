<?php
namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="court_cases")
 */
class DSCourtCase
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DSCompanyList
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCompanyList")
     * @ORM\JoinColumn(name="company_list_id", referencedColumnName="id")
     */
    private $companyList;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $data;

    /**
     * @var string
     * @ORM\Column(type="string", name="case_number")
     */
    private $caseNumber;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="case_date")
     */
    private $caseDate;

    /**
     * @var string
     * @ORM\Column(type="string", name="case_type")
     */
    private $caseType;

    /**
     * @var float
     * @ORM\Column(type="decimal", scale=12, precision=2)
     */
    private $sum;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="is_active")
     */
    private $isActive;

    /**
     * @var string
     * @ORM\Column(type="string", name="current_instance")
     */
    private $currentInstance;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="instance_date")
     */
    private $instanceDate;

    /**
     * @var DSCourtCaseSide[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DSCourtCaseSide", mappedBy="courtCase")
     */
    private $sides;

    public function __construct()
    {
        $this->sides = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DSCompanyList
     */
    public function getCompanyList(): DSCompanyList
    {
        return $this->companyList;
    }

    /**
     * @param DSCompanyList $companyList
     * @return DSCourtCase
     */
    public function setCompanyList(DSCompanyList $companyList): DSCourtCase
    {
        $this->companyList = $companyList;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return DSCourtCase
     */
    public function setData(array $data): DSCourtCase
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return string
     */
    public function getCaseNumber(): string
    {
        return $this->caseNumber;
    }

    /**
     * @param string $caseNumber
     * @return DSCourtCase
     */
    public function setCaseNumber(string $caseNumber): DSCourtCase
    {
        $this->caseNumber = $caseNumber;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCaseDate(): \DateTime
    {
        return $this->caseDate;
    }

    /**
     * @param \DateTime $caseDate
     * @return DSCourtCase
     */
    public function setCaseDate(\DateTime $caseDate): DSCourtCase
    {
        $this->caseDate = $caseDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCaseType(): string
    {
        return $this->caseType;
    }

    /**
     * @param string $caseType
     * @return DSCourtCase
     */
    public function setCaseType(string $caseType): DSCourtCase
    {
        $this->caseType = $caseType;
        return $this;
    }

    /**
     * @return float
     */
    public function getSum(): float
    {
        return $this->sum;
    }

    /**
     * @param float $sum
     * @return DSCourtCase
     */
    public function setSum(float $sum): DSCourtCase
    {
        $this->sum = $sum;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return DSCourtCase
     */
    public function setIsActive(bool $isActive): DSCourtCase
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentInstance(): string
    {
        return $this->currentInstance;
    }

    /**
     * @param string $currentInstance
     * @return DSCourtCase
     */
    public function setCurrentInstance(string $currentInstance): DSCourtCase
    {
        $this->currentInstance = $currentInstance;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInstanceDate(): \DateTime
    {
        return $this->instanceDate;
    }

    /**
     * @param \DateTime $instanceDate
     * @return DSCourtCase
     */
    public function setInstanceDate(\DateTime $instanceDate): DSCourtCase
    {
        $this->instanceDate = $instanceDate;
        return $this;
    }

    /**
     * @return DSCourtCaseSide[]|ArrayCollection
     */
    public function getSides()
    {
        return $this->sides;
    }
}