<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="contracts")
 */
class DSContract
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DSCompanyList
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCompanyList")
     * @ORM\JoinColumn(name="company_list_id", referencedColumnName="id")
     *
     */
    private $companyList;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $data;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="contract_number")
     */
    private $contractNumber;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $subject;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="law_number")
     */
    private $lawNumber;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @var int
     * @ORM\Column(type="integer",name="status_code")
     */
    private $statusCode;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", name="term_date")
     */
    private $termDate;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", name="contract_date")
     */
    private $contractDate;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", name="cancel_date")
     */
    private $cancelDate;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", name="exec_date")
     */
    private $execDate;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=12, precision=2)
     */
    private $sum;

    /**
     * @var DSContractConformity
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSContractConformity")
     * @ORM\JoinColumn(name="id", referencedColumnName="contract_id", )
     *
     */
    private $satisfied;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DSCompanyList
     */
    public function getCompanyList(): DSCompanyList
    {
        return $this->companyList;
    }

    /**
     * @param DSCompanyList $companyList
     * @return DSContract
     */
    public function setCompanyList(DSCompanyList $companyList): DSContract
    {
        $this->companyList = $companyList;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return DSContract
     */
    public function setData(array $data): DSContract
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getContractNumber(): ?string
    {
        return $this->contractNumber;
    }

    /**
     * @param null|string $contractNumber
     * @return DSContract
     */
    public function setContractNumber(?string $contractNumber): DSContract
    {
        $this->contractNumber = $contractNumber;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param null|string $subject
     * @return DSContract
     */
    public function setSubject(?string $subject): DSContract
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getLawNumber(): ?string
    {
        return $this->lawNumber;
    }

    /**
     * @param null|string $lawNumber
     * @return DSContract
     */
    public function setLawNumber(?string $lawNumber): DSContract
    {
        $this->lawNumber = $lawNumber;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param null|string $status
     * @return DSContract
     */
    public function setStatus(?string $status): DSContract
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getTermDate(): ?\DateTime
    {
        return $this->termDate;
    }

    /**
     * @param \DateTime|null $termDate
     * @return DSContract
     */
    public function setTermDate(?\DateTime $termDate): DSContract
    {
        $this->termDate = $termDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getContractDate(): ?\DateTime
    {
        return $this->contractDate;
    }

    /**
     * @param \DateTime|null $contractDate
     * @return DSContract
     */
    public function setContractDate(?\DateTime $contractDate): DSContract
    {
        $this->contractDate = $contractDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCancelDate(): ?\DateTime
    {
        return $this->cancelDate;
    }

    /**
     * @param \DateTime|null $cancelDate
     * @return DSContract
     */
    public function setCancelDate(?\DateTime $cancelDate): DSContract
    {
        $this->cancelDate = $cancelDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getExecDate(): ?\DateTime
    {
        return $this->execDate;
    }

    /**
     * @param \DateTime|null $execDate
     * @return DSContract
     */
    public function setExecDate(?\DateTime $execDate): DSContract
    {
        $this->execDate = $execDate;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getSum(): ?float
    {
        return $this->sum;
    }

    /**
     * @param float|null $sum
     * @return DSContract
     */
    public function setSum(?float $sum): DSContract
    {
        $this->sum = $sum;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     * @return DSContract
     */
    public function setStatusCode(int $statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSatisfied()
    {
        return $this->satisfied;
    }




}