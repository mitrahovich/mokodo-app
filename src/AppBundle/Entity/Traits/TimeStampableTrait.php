<?php
namespace AppBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait TimeStampableTrait
{
    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(type="datetime", options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $updated;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(type="datetime", options={"default" : "CURRENT_TIMESTAMP"})
     */
    private $created;

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    public function getCreatedDateString()
    {
        return $this->getCreated()->format('d.m.Y');
    }

    public function getCreatedDateTimeString()
    {
        return $this->getCreated()->format('d.m.Y H:i:s');
    }
}