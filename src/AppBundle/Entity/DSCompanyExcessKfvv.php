<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DSCompanyList
 *
 * @ORM\Table(name="company_excess_kfvv")
 * @ORM\Entity
 */
class DSCompanyExcessKfvv
{
    /**
     * @ORM\Id
     * @var DSCompanyList
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCompanyList")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     *
     */
    private $company;

    /**
     * @var int
     * @ORM\Column(type="integer", name="excess")
     */
    private $isExcess;

    /**
     * @return DSCompanyList
     */
    public function getCompany(): DSCompanyList
    {
        return $this->company;
    }

    /**
     * @param DSCompanyList $company
     */
    public function setCompany(DSCompanyList $company)
    {
        $this->company = $company;
    }

    /**
     * @return integer
     */
    public function isExcess(): int
    {
        return $this->isExcess;
    }

    /**
     * @param int $isExcess
     * @return void
     */
    public function setIsExcess(int $isExcess): void
    {
        $this->isExcess = $isExcess;
    }


}