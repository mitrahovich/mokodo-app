<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="company_card")
 */
class DSCompanyCard
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DSCompanyList
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCompanyList")
     * @ORM\JoinColumn(name="company_list_id", referencedColumnName="id")
     *
     */
    private $companyList;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $data;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $ogrn;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @var int|null
     * @ORM\Column(type="integer", name="region_code")
     */
    private $regionCode;

    /**
     * @var int|null
     * @ORM\Column(type="integer", name="companies_at_this_address")
     */
    private $companiesAtThisAddress;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=16, precision=2)
     */
    private $capital;

    /**
     * @var int|null
     * @ORM\Column(type="integer", name="trustworthiness_index")
     */
    private $trustworthinessIndex;

    /**
     * @var DSCompanyPhone[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DSCompanyPhone", mappedBy="companyCard")
     */
    private $phones;

    /**
     * @var DSCompanyEmail[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DSCompanyEmail", mappedBy="companyCard")
     */
    private $emails;

    public function __construct()
    {
        $this->phones = new ArrayCollection();
        $this->emails = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DSCompanyList
     */
    public function getCompanyList(): DSCompanyList
    {
        return $this->companyList;
    }

    /**
     * @param DSCompanyList $companyList
     * @return DSCompanyCard
     */
    public function setCompanyList(DSCompanyList $companyList): DSCompanyCard
    {
        $this->companyList = $companyList;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return DSCompanyCard
     */
    public function setData(array $data): DSCompanyCard
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return DSCompanyCard
     */
    public function setName(?string $name): DSCompanyCard
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getOgrn(): ?string
    {
        return $this->ogrn;
    }

    /**
     * @param null|string $ogrn
     * @return DSCompanyCard
     */
    public function setOgrn(?string $ogrn): DSCompanyCard
    {
        $this->ogrn = $ogrn;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param null|string $status
     * @return DSCompanyCard
     */
    public function setStatus(?string $status): DSCompanyCard
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param null|string $address
     * @return DSCompanyCard
     */
    public function setAddress(?string $address): DSCompanyCard
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRegionCode(): int
    {
        return $this->regionCode;
    }

    /**
     * @param int|null $regionCode
     * @return DSCompanyCard
     */
    public function setRegionCode(?int $regionCode): DSCompanyCard
    {
        $this->regionCode = $regionCode;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCompaniesAtThisAddress(): ?int
    {
        return $this->companiesAtThisAddress;
    }

    /**
     * @param int|null $companiesAtThisAddress
     * @return DSCompanyCard
     */
    public function setCompaniesAtThisAddress(?int $companiesAtThisAddress): DSCompanyCard
    {
        $this->companiesAtThisAddress = $companiesAtThisAddress;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCapital(): ?float
    {
        return $this->capital;
    }

    /**
     * @param float|null $capital
     * @return DSCompanyCard
     */
    public function setCapital(?float $capital): DSCompanyCard
    {
        $this->capital = $capital;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTrustworthinessIndex(): ?int
    {
        return $this->trustworthinessIndex;
    }

    /**
     * @param int|null $trustworthinessIndex
     * @return DSCompanyCard
     */
    public function setTrustworthinessIndex(?int $trustworthinessIndex): DSCompanyCard
    {
        $this->trustworthinessIndex = $trustworthinessIndex;
        return $this;
    }

    /**
     * @return DSCompanyPhone[]|ArrayCollection
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * @return DSCompanyEmail[]|ArrayCollection
     */
    public function getEmails()
    {
        return $this->emails;
    }

}