<?php
namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimeStampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * DataSource
 *
 * @ORM\Table(name="data_source_list")
 * @ORM\Entity
 */
class DataSource
{
    use TimeStampableTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     *
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string")
     *
     */
    private $src;

    /**
     * @var string
     *
     * @ORM\Column(name="file_db", type="string")
     *
     */
    private $fileDb;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @param string $src
     * @return DataSource
     */
    public function setSrc($src)
    {
        $this->src = $src;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileDb(): string
    {
        return $this->fileDb;
    }

    /**
     * @param string $fileDb
     */
    public function setFileDb(string $fileDb)
    {
        $this->fileDb = $fileDb;
    }


}