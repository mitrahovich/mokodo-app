<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="court_case_sides")
 */
class DSCourtCaseSide
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DSCourtCase
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCourtCase")
     * @ORM\JoinColumn(name="court_case_id", referencedColumnName="id")
     *
     */
    private $courtCase;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $data;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $ogrn;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $inn;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DSCourtCase
     */
    public function getCourtCase(): DSCourtCase
    {
        return $this->courtCase;
    }

    /**
     * @param DSCourtCase $courtCase
     * @return DSCourtCaseSide
     */
    public function setCourtCase(DSCourtCase $courtCase): DSCourtCaseSide
    {
        $this->courtCase = $courtCase;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return DSCourtCaseSide
     */
    public function setData(array $data): DSCourtCaseSide
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return DSCourtCaseSide
     */
    public function setName(string $name): DSCourtCaseSide
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int|null $type
     * @return DSCourtCaseSide
     */
    public function setType(?int $type): DSCourtCaseSide
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOgrn(): ?string
    {
        return $this->ogrn;
    }

    /**
     * @param string|null $ogrn
     * @return DSCourtCaseSide
     */
    public function setOgrn(?string $ogrn): DSCourtCaseSide
    {
        $this->ogrn = $ogrn;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInn(): ?string
    {
        return $this->inn;
    }

    /**
     * @param string|null $inn
     * @return DSCourtCaseSide
     */
    public function setInn(?string $inn): DSCourtCaseSide
    {
        $this->inn = $inn;
        return $this;
    }

    /**
     * @return string
     */
    public function getTypeName(): string
    {
        switch ($this->type) {
            case 0:
                return 'В качестве истца';
                break;
            case 1:
                return 'В качестве ответчика';
                break;
            case 2:
                return 'В качестве иного лица';
                break;
            case 3:
                return 'В качестве третьего лица';
                break;
            case 4:
                return 'В любом качестве';
                break;
            default:
                return 'Не определено';
        }

    }

}