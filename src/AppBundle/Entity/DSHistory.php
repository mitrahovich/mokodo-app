<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="data_source_history")
 */
class DSHistory
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DSContract
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSContract")
     * @ORM\JoinColumn(name="contract_id", referencedColumnName="id")
     */
    private $contract;

    /**
     * @var DSDictHistoryOperation;
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSDictHistoryOperation")
     * @ORM\JoinColumn(name="ds_history_operation_id", referencedColumnName="id")
     */
    private $operation;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime")
     */
    private $readed;

    /**
     * DSHistory constructor.
     * @param DSContract $contract
     * @param DSDictHistoryOperation $operation
     */
    public function __construct(DSContract $contract, DSDictHistoryOperation $operation)
    {
        $this->contract = $contract;
        $this->operation = $operation;
        $this->created = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return DSHistory
     */
    public function setCreated(\DateTime $created): DSHistory
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getReaded(): ?\DateTime
    {
        return $this->readed;
    }

    /**
     * @param \DateTime $readed
     * @return DSHistory
     */
    public function setReaded(\DateTime $readed): DSHistory
    {
        $this->readed = $readed;
        return $this;
    }

    /**
     * @return DSContract
     */
    public function getContract(): DSContract
    {
        return $this->contract;
    }

    /**
     * @param DSContract $contract
     * @return DSHistory
     */
    public function setContract(DSContract $contract): DSHistory
    {
        $this->contract = $contract;
        return $this;
    }

    /**
     * @return DSDictHistoryOperation
     */
    public function getOperation(): DSDictHistoryOperation
    {
        return $this->operation;
    }

    /**
     * @param DSDictHistoryOperation $operation
     * @return DSHistory
     */
    public function setOperation(DSDictHistoryOperation $operation): DSHistory
    {
        $this->operation = $operation;
        return $this;
    }

}