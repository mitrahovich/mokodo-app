<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="bankruptcy")
 */
class DSBankruptcy
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DSCompanyList
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCompanyList")
     * @ORM\JoinColumn(name="company_list_id", referencedColumnName="id")
     *
     */
    private $companyList;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $data;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", name="publish_date")
     */
    private $publishDate;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="number")
     */
    private $number;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var string|null
     * @ORM\Column(type="string", name="case_number")
     */
    private $caseNumber;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $text;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DSCompanyList
     */
    public function getCompanyList(): DSCompanyList
    {
        return $this->companyList;
    }

    /**
     * @param DSCompanyList $companyList
     * @return DSBankruptcy
     */
    public function setCompanyList(DSCompanyList $companyList): DSBankruptcy
    {
        $this->companyList = $companyList;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return DSBankruptcy
     */
    public function setData(array $data): DSBankruptcy
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getPublishDate(): ?\DateTime
    {
        return $this->publishDate;
    }

    /**
     * @param \DateTime|null $publishDate
     * @return DSBankruptcy
     */
    public function setPublishDate(?\DateTime $publishDate): DSBankruptcy
    {
        $this->publishDate = $publishDate;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param null|string $number
     * @return DSBankruptcy
     */
    public function setNumber(?string $number): DSBankruptcy
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param null|string $type
     * @return DSBankruptcy
     */
    public function setType(?string $type): DSBankruptcy
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCaseNumber(): ?string
    {
        return $this->caseNumber;
    }

    /**
     * @param null|string $caseNumber
     * @return DSBankruptcy
     */
    public function setCaseNumber(?string $caseNumber): DSBankruptcy
    {
        $this->caseNumber = $caseNumber;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param null|string $text
     * @return DSBankruptcy
     */
    public function setText(?string $text): DSBankruptcy
    {
        $this->text = $text;
        return $this;
    }

}