<?php
namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class DSCompanyListRepository
 * @package AppBundle\Entity\Repository
 */
class DSCompanyListRepository extends EntityRepository
{
    /**
     * @param string $inn
     * @return array
     */
    public function findByInn(string $inn): array
    {
        return $this->createQueryBuilder('dscompanyList')
            ->where('dscompanyList.inn LIKE :inn')
            ->setParameter('inn', '%' . $inn . '%')
            ->getQuery()
            ->getResult();
    }
}