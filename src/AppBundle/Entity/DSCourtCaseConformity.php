<?php
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="court_cases_conformity")
 */
class DSCourtCaseConformity
{


    /**
     * @ORM\Id
     * @var DSCourtCase
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCourtCase")
     * @ORM\JoinColumn(name="court_cases_id", referencedColumnName="id")
     *
     */
    private $courtCase;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="conformity")
     */
    private $conformity;


    /**
     * @return DSCourtCase
     */
    public function getCourtCase(): DSCourtCase
    {
        return $this->courtCase;
    }

    /**
     * @param DSCourtCase $courtCase
     */
    public function setCourtCase(DSCourtCase $courtCase)
    {
        $this->courtCase = $courtCase;
    }

    /**
     * @return bool
     */
    public function isConformity(): bool
    {
        return $this->conformity;
    }

    /**
     * @param bool $conformity
     */
    public function setConformity(bool $conformity)
    {
        $this->conformity = $conformity;
    }

}