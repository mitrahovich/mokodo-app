<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="dict_vv_level")
 */
class DSDictVVLevel
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", name="deposit", scale=12, precision=2)
     */
    private $deposit;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", name="commitment", scale=12, precision=2)
     */
    private $commitment;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return float|null
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * @param float|null $deposit
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * @return float|null
     */
    public function getCommitment()
    {
        return $this->commitment;
    }

    /**
     * @param float|null $commitment
     */
    public function setCommitment($commitment)
    {
        $this->commitment = $commitment;
    }
}