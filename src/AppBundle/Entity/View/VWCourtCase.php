<?php
namespace AppBundle\Entity\View;


use AppBundle\Entity\DSCompanyList;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="vw_card_court_cases")
 */
class VWCourtCase
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DSCompanyList
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\DSCompanyList")
     * @ORM\JoinColumn(name="company_list_id", referencedColumnName="id")
     */
    private $companyList;

    /**
     * @var string
     * @ORM\Column(type="string", name="defendant")
     */
    private $defendant;

    /**
     * @var string
     * @ORM\Column(type="string", name="defendant_inn")
     */
    private $defendantInn;

    /**
     * @var string
     * @ORM\Column(type="string", name="claimant")
     */
    private $claimant;

    /**
     * @var string
     * @ORM\Column(type="string", name="case_number")
     */
    private $caseNumber;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="case_date")
     */
    private $caseDate;

    /**
     * @var string
     * @ORM\Column(type="string", name="case_type")
     */
    private $caseType;

    /**
     * @var float
     * @ORM\Column(type="decimal", scale=12, precision=2)
     */
    private $sum;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="is_active")
     */
    private $isActive;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="instance_date")
     */
    private $instanceDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="satisfied", type="boolean")
     */
    private $satisfied;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DSCompanyList
     */
    public function getCompanyList(): DSCompanyList
    {
        return $this->companyList;
    }

    /**
     * @return string
     */
    public function getCaseNumber(): string
    {
        return $this->caseNumber;
    }


    /**
     * @return \DateTime
     */
    public function getCaseDate(): \DateTime
    {
        return $this->caseDate;
    }

    /**
     * @return string
     */
    public function getDefendant(): string
    {
        return $this->defendant;
    }

    /**
     * @return string
     */
    public function getDefendantInn(): string
    {
        return $this->defendantInn;
    }


    /**
     * @return string
     */
    public function getClaimant(): string
    {
        return $this->claimant;
    }


    public function getCaseType(): string
    {
        return $this->caseType;
    }

    /**
     * @return float
     */
    public function getSum(): float
    {
        return $this->sum;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @return \DateTime
     */
    public function getInstanceDate(): \DateTime
    {
        return $this->instanceDate;
    }

    /**
     * @return bool
     */
    public function isSatisfied(): bool
    {
        return $this->satisfied;
    }

}