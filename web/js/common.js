$(document).ready(function() {

    $( ".ds-select" ).select2( {
        theme: "default",
        placeholder : 'Источник данных',
        ajax: {
            url: '/ds/listjson',
            dataType: 'json'
        }
    } );

    $( ".ds-select" ).on( "change", function() {
        $.getJSON('/ds/set/'+$( this ).val(), {}, function(json){
            if( true === json.result){
                location.reload();
            }
        });
    });

    $.fn.dataTable.moment = function ( format, locale ) {
        var types = $.fn.dataTable.ext.type;

        // Add type detection
        types.detect.unshift( function ( d ) {
            return moment( d, format, locale, true ).isValid() ?
                'moment-'+format :
                null;
        } );

        // Add sorting method - use an integer for the sorting
        types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
            return moment( d, format, locale, true ).unix();
        };
    };

    $.get("/search/company/list/all", function(data){
        $("#search_input").typeahead({
            source:data,
            afterSelect: function(item) {
                console.log(item);
                window.location.href = item.url;
            }
        });
    },'json');

});

function changePage(url) {
    window.location.href = url;
}
